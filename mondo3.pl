

mondo(24,11).


%stanze
st(rect(p(0,0),p(6,3)),1).     %S1
st(rect(p(9,0),p(15,3)),2).    %S2
st(rect(p(18,0),p(24,3)),3).   %S3
st(rect(p(0,4),p(6,7)),4).     %S4
st(rect(p(9,4),p(15,7)),5).    %S5
st(rect(p(18,4),p(24,7)),6).   %S6
st(rect(p(0,8),p(6,11)),7).    %S7
st(rect(p(9,8),p(15,11)),8).   %S8
st(rect(p(18,8),p(24,11)),9).  %s9

% informazioni sulle stanze
info(st(rect(p(0,0),p(6,3)),1), bianco).
info(st(rect(p(9,0),p(15,3)),2), bianco).
info(st(rect(p(18,0),p(24,3)),3), rosso).
info(st(rect(p(0,4),p(6,7)),4), bianco).
info(st(rect(p(9,4),p(15,7)),5), rosso).
info(st(rect(p(18,4),p(24,7)),6), rosso).
info(st(rect(p(0,8),p(6,11)),7), bianco).
info(st(rect(p(9,8),p(15,11)),8), bianco).
info(st(rect(p(18,8),p(24,11)),9), rosso).

%connessioni stanza 1
conn_st(st(rect(p(0,0),p(6,3)),1),[st(rect(p(9,0),p(15,3)),2),
				   st(rect(p(0,4),p(6,7)),4)]).


%connessioni stanza 2
conn_st(st(rect(p(9,0),p(15,3)),2),[st(rect(p(0,0),p(6,3)),1),
				    st(rect(p(9,4),p(15,7)),5)]).

%connessioni stanza 3
conn_st(st(rect(p(18,0),p(24,3)),3),[st(rect(p(18,4),p(24,7)),6)]).

%connessioni stanza 4
conn_st(st(rect(p(0,4),p(6,7)),4),[st(rect(p(0,0),p(6,3)),1),
				   st(rect(p(0,8),p(6,11)),7)]).

%connessioni stanza 5
conn_st(st(rect(p(9,4),p(15,7)),5),[st(rect(p(9,0),p(15,3)),2),
				    st(rect(p(18,4),p(24,7)),6),
				    st(rect(p(9,8),p(15,11)),8)]).

%connessioni stanza 6
conn_st(st(rect(p(18,4),p(24,7)),6),[st(rect(p(18,0),p(24,3)),3),
				     st(rect(p(9,4),p(15,7)),5)]).

%connessioni stanza 7
conn_st(st(rect(p(0,8),p(6,11)),7),[st(rect(p(0,4),p(6,7)),4),
				    st(rect(p(9,8),p(15,11)),8)]).

%connessioni stanza 8
conn_st(st(rect(p(9,8),p(15,11)),8),[st(rect(p(9,4),p(15,7)),5),
				     st(rect(p(0,8),p(6,11)),7),
				     st(rect(p(18,8),p(24,11)),9)]).

%connessioni stanza 9
conn_st(st(rect(p(18,8),p(24,11)),9),[st(rect(p(9,8),p(15,11)),8)]).


%stanza in cui si trova il wumpus S6
wumpus(st(rect(p(18,4),p(24,7)),6)).
pozzo(null).

conosce_nel_mondo_caricato(null).



