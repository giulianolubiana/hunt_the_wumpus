mondo(33,19).


st(rect(p(0,0),p(6,3)),1).     %S1
st(rect(p(9,0),p(15,3)),2).    %S2
st(rect(p(18,0),p(24,3)),3).   %S3
st(rect(p(27,0),p(33,3)),4).   %S4
st(rect(p(0,4),p(6,7)),5).     %S5
st(rect(p(9,4),p(15,7)),6).    %S6
st(rect(p(18,4),p(24,7)),7).   %S7
st(rect(p(27,4),p(33,7)),8).   %S8
st(rect(p(0,8),p(6,11)),9).    %S9
st(rect(p(9,8),p(15,11)),10).  %S10
st(rect(p(18,8),p(24,11)),11). %S11
st(rect(p(27,8),p(33,11)),12). %S12
st(rect(p(0,12),p(6,15)),13).  %S13
st(rect(p(9,12),p(15,15)),14). %S14
st(rect(p(18,12),p(24,15)),15).%S15
st(rect(p(27,12),p(33,15)),16).%S16
st(rect(p(0,16),p(6,19)),17).  %S17
st(rect(p(9,16),p(15,19)),18). %S18
st(rect(p(18,16),p(24,19)),19).%S19
st(rect(p(27,16),p(33,19)),20).%S20


% informazioni sulle stanze
info(st(rect(p(0,0),p(6,3)),1), bianco).
info(st(rect(p(9,0),p(15,3)),2), bianco).
info(st(rect(p(18,0),p(24,3)),3), rosso).
info(st(rect(p(27,0),p(33,3)),4), bianco).
info(st(rect(p(0,4),p(6,7)),5), bianco).
info(st(rect(p(9,4),p(15,7)),6), rosso).
info(st(rect(p(18,4),p(24,7)),7), rosso).
info(st(rect(p(27,4),p(33,7)),8), rosso).
info(st(rect(p(0,8),p(6,11)),9), bianco).
info(st(rect(p(9,8),p(15,11)),10),bianco).
info(st(rect(p(18,8),p(24,11)),11),rosso).
info(st(rect(p(27,8),p(33,11)),12),bianco).
info(st(rect(p(0,12),p(6,15)),13),verde).
info(st(rect(p(9,12),p(15,15)),14),bianco).
info(st(rect(p(18,12),p(24,15)),15),bianco).
info(st(rect(p(27,12),p(33,15)),16),bianco).
info(st(rect(p(0,16),p(6,19)),17),verde).
info(st(rect(p(9,16),p(15,19)),18),verde).
info(st(rect(p(18,16),p(24,19)),19),bianco).
info(st(rect(p(27,16),p(33,19)),20),bianco).

% connessioni stanza 1
conn_st(st(rect(p(0,0),p(6,3)),1),[st(rect(p(0,4),p(6,7)),5)]).

% connessioni stanza 2
conn_st(st(rect(p(9,0),p(15,3)),2),[st(rect(p(18,0),p(24,3)),3),
				    st(rect(p(9,4),p(15,7)),6)]).

% connessioni stanza 3
conn_st(st(rect(p(18,0),p(24,3)),3),[st(rect(p(9,0),p(15,3)),2),
				     st(rect(p(27,0),p(33,3)),4),
				     st(rect(p(18,4),p(24,7)),7)]).

% connessioni stanza 4
conn_st(st(rect(p(27,0),p(33,3)),4),[st(rect(p(18,0),p(24,3)),3),
				     st(rect(p(27,4),p(33,7)),8)]).

% connessioni stanza 5
conn_st(st(rect(p(0,4),p(6,7)),5), [st(rect(p(0,0),p(6,3)),1),
				    st(rect(p(9,4),p(15,7)),6),
				    st(rect(p(0,8),p(6,11)),9)]).

% connessioni stanza 6
conn_st(st(rect(p(9,4),p(15,7)),6),[st(rect(p(0,4),p(6,7)),5),
				     st(rect(p(9,0),p(15,3)),2),
				     st(rect(p(9,8),p(15,11)),10)]).

% connessioni stanza 7
conn_st(st(rect(p(18,4),p(24,7)),7), [st(rect(p(18,0),p(24,3)),3),
				     st(rect(p(18,8),p(24,11)),11)]).

% connessioni stanza 8
conn_st(st(rect(p(27,4),p(33,7)),8), [st(rect(p(27,0),p(33,3)),4),
				      st(rect(p(27,8),p(33,11)),12)]).

% connessioni stanza 9
conn_st(st(rect(p(0,8),p(6,11)),9), [st(rect(p(0,4),p(6,7)),5),
				     st(rect(p(9,8),p(15,11)),10),
				     st(rect(p(0,12),p(6,15)),13)]).

% connessioni stanza 10
conn_st(st(rect(p(9,8),p(15,11)),10), [st(rect(p(9,4),p(15,7)),6),
				       st(rect(p(0,8),p(6,11)),9),
				       st(rect(p(18,8),p(24,11)),11),
				       st(rect(p(9,12),p(15,15)),14)]).

% connesioni stanza 11
conn_st(st(rect(p(18,8),p(24,11)),11), [st(rect(p(18,4),p(24,7)),7),
				        st(rect(p(9,8),p(15,11)),10),
				        st(rect(p(27,8),p(33,11)),12),
				        st(rect(p(18,12),p(24,15)),15)]).

% connessioni stanza 12
conn_st(st(rect(p(27,8),p(33,11)),12), [st(rect(p(27,4),p(33,7)),8),
					st(rect(p(18,8),p(24,11)),11)]).

% connessioni stanza 13
conn_st(st(rect(p(0,12),p(6,15)),13), [st(rect(p(0,8),p(6,11)),9),
				       st(rect(p(9,12),p(15,15)),14),
				       st(rect(p(0,16),p(6,19)),17)]).

% connessioni stanza 14
conn_st(st(rect(p(9,12),p(15,15)),14), [st(rect(p(9,8),p(15,11)),10),
				       st(rect(p(0,12),p(6,15)),13),
				       st(rect(p(18,12),p(24,15)),15),
				       st(rect(p(9,16),p(15,19)),18)]).

% connessioni stanza 15
conn_st(st(rect(p(18,12),p(24,15)),15), [st(rect(p(18,8),p(24,11)),11),
					 st(rect(p(9,12),p(15,15)),14),
					 st(rect(p(27,12),p(33,15)),16),
					 st(rect(p(18,16),p(24,19)),19)]).

% connessioni stanza 16
conn_st(st(rect(p(27,12),p(33,15)),16), [st(rect(p(18,12),p(24,15)),15)]).

%connessioni stanza 17
conn_st(st(rect(p(0,16),p(6,19)),17), [st(rect(p(0,12),p(6,15)),13),
				       st(rect(p(9,16),p(15,19)),18)]).

% connessioni stanza 18
conn_st(st(rect(p(9,16),p(15,19)),18), [st(rect(p(9,12),p(15,15)),14),
					st(rect(p(0,16),p(6,19)),17)]).

% connessioni stanza 19
conn_st(st(rect(p(18,16),p(24,19)),19), [st(rect(p(18,12),p(24,15)),15),
					 st(rect(p(27,16),p(33,19)),20)]).

% connessioni stanza 20
conn_st(st(rect(p(27,16),p(33,19)),20), [st(rect(p(18,16),p(24,19)),19)]).

wumpus(st(rect(p(18,4),p(24,7)),7)).

pozzo(st(rect(p(0,16),p(6,19)),17)).

conosce_nel_mondo_caricato(null).









