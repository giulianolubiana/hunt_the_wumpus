:- use_module('../tpl/syntax').
:- style_check(-discontiguous).


/******************************************************************/

predefined_type lists.

open_type stato.
    % aperto, definisce gli stati dell'agente
open_type azione.
    % aperto, definisce le azioni possibili dell'agente
open_type problema.
    % aperto, definisce i problemi che possono causare
    % la non attuazione di una azione pianificata
type stato_pianificazione :=
    {stato} ; {interruzione_passo}.
    %  a livello di pianificazione, l'agente e' in un suo stato o in
    %  uno stato di interruzione di una decisione
type stato_decisione :=
    successo;  fallimento;
    {stato} ; {interruzione}.
    %  a livello di decisione, l'agente ha raggiunto lo stato
    %  di successo o fallimento, oppure e' in un suo stato o in
    %  uno di interruzione del sottostante livello di pianificazione
type interruzione :=
                int_piano(stato, list(stato), interruzione_passo).
    % un'interruzione di un passo di decisione e':
    % int_piano(S, [S'|Decisione], Causa):  il passo S->S' e'
    % stato interrotto per la causa indicata
type interruzione_passo :=
                da_ridecidere;
                int_azione(stato, list(stato), list(problema)).
    % un'interruzione di una decisione puo' essere:
    %  a) da_ridecidere se l'agente non trova un piano
    %  b) int_azione(I, [I'|Piano], Problemi):  l'azione A:I->I' e'
    %     stata interrotta per Probelmi dal sottostante livello  di
    %     attuazione; [I'|Piano] e' la parte di piano non eseguita



%**   1.  CICLO  DECISIONE-PIANIFICAZIONE-ATTUAZIONE   *******************/

%-------------------------  DECISIONE
%
import_pred obiettivo(stato_decisione).
%  obiettivo(+S):   S e' uno ``stato obiettivo''

import_pred riconsidera(stato, list(stato), interruzione_passo, list(stato)).
%  riconsidera(+S, +Restante, +Causa, -Decisione)
%  Pre:  l'esecuzione di una decisone e' stata interrotta nello
%          stato S, Restante e' la sequenza di decisioni non attuata
%          Causa e' la causa dell'interruzione
%  Post:  Decisione e' lanuova sequenza di decisioni

import_pred prendi_decisione(stato, list(stato)).
%  prendi_decisione(+S,-Dec):   Dec e' la sequenza di decisoni

%  presa nello stato S in base alla conoscenza attuale dell'agente

pred decisione(stato_decisione, stato_decisione).
%  decisione(+S0, -SF):   in base alle conoscenze nello stato S0 prende una
%  decisione e passa alla pianificazione ed attuazione; termina il
%  processo con SF=successo se raggiunge uno stato obiettivo o con
%  SF=fallimento se raggiunge uno stato in cui non e' in grado di decidere
%
decisione(S0, successo) :-
      obiettivo(S0),!,
      traccia(decisione_1, obiettivo(S0)).
decisione(S0,SF) :-
      decidi(S0, Decisione),
      traccia(decisione_2, trc(exit:decidi(S0, Decisione))),
      pianificazione(Decisione, S1), !, %passa al livello pianificazione
      traccia(decisione_3, trc(call:decisione(S1,SF))),
      decisione(S1,SF).
%  arriva qui solo se decisione(S0,Decisione) e' fallito
decisione(S0, fallimento) :-
      traccia(decisione_4, fallimento:S0).

pred decidi(stato_decisione, list(stato)).
%   decidi(+S, -Dec) : Dec e' la decisione presa nello stato S
% caso 1: interruzione. Riconsidera la decisione interrotta
decidi(int_piano(S, Piano, Causa), Decisione) :- !,
     traccia(decidi, trc(call:riconsidera(S, Piano, Causa, Decisione))),
     riconsidera(S, Piano, Causa, Decisione).
% caso 2: nuovo stato. Osserva lo stato e apprende e prende una nuova
% decisione
decidi(S, Decisione) :-
     osserva_e_impara(S),
     %traccia(decidi, call:prendi_decisione(S,Decisione)),
     prendi_decisione(S, Decisione).


%-------------------------  PIANIFICAZIONE

import_pred calcola_piano(stato, stato, list(stato)).
% calcola_piano(+S1, +S2, -Piano):
%  Piano e' un piano per passare da S1 ad S2,
%  valutato nello stato di conoscenza in S1

pred pianificazione(list(stato), stato_decisione).
%  pianificazione(+Decisione,-SF):  SF e' lo stato (di successo o di
%  interruzione) in cui terminano le fasi di pianificazione ed
%  attuazione della sequenza di Decisione
%
pianificazione([S0,S1|Decisione], SF) :-
     %traccia(pianificazione:0, call:calcola_piano(S0, S1, Piano)),
     calcola_piano(S0, S1, Piano) ->
     traccia(pianificazione_1, trc(exit:calcola_piano(S0, S1, Piano))),
     attuazione([S0|Piano], EsitoAttuazione),  % passa al livello attuazione
     traccia(pianificazione_2, exit:attuazione),
     (	 EsitoAttuazione=S1 ->
	 osserva_e_impara(S1), % osseva e apprende prima di proseguire
	 pianificazione([S1|Decisione], SF)
         ;
         traccia(pianificazione_3,
		 trc(int_piano(S0, [S1|Decisione],EsitoAttuazione))),
	 SF=int_piano(S0, [S1|Decisione],EsitoAttuazione)
     )
     ;
     traccia(pianificazione_4, trc(int_piano(S0, [S1|Decisione], da_ridecidere))),
     SF = int_piano(S0, [S1|Decisione], da_ridecidere).
pianificazione([Sn], Sn).


%-------------------------  ATTUAZIONE

import_pred attuabile(azione, stato, stato, number, list(assumibile)).
%  attuabile(?A, +S1, ?S2, ?C, ?Ass) :  assumendo Ass, vi e'
%  una azione A che passa dallo stato S1 allo stato S2 con
%  costo C.   NB usato in pianificazione e attuazione


import_pred esegui_azione(azione, stato, stato).
%  esegui_azione(+A, +S1, +S2) :  l'agente esegue l'azione
%  nel mondo; va implementato a livello di visualizzazione
%  del comportamento dell'agente nel tempo
%

pred attuazione(list(stato), stato_pianificazione).
%  pianificazione(+Piano,-SF):  SF e' lo stato (di successo o di
%  interruzione) in cui termina la fase di attuazione del Piano
%
attuazione([I0,I1|Piano], SF) :-
      attuabile(A, I0, I1, Costo, Assunzioni),
      traccia(attuazione_1, trc(exit:attuabile(A, I0, I1, Costo, Assunzioni))),
      valuta(Assunzioni, Problemi),
      traccia(attuazione_2, trc(exit: valuta(Assunzioni, Problemi))),
      ( Problemi=[] ->
        esegui_azione(A, I0, I1), % agisce
	traccia(attuazione_3, trc(exit:esegui_azione(A,I0,I1))),
        osserva_e_impara(I1), % osserva e apprende prima di proseguire
        attuazione([I1|Piano], SF)
      ;
        traccia(attuazione_4, trc(int_azione(I0,  [I1|Piano], Problemi))),
        SF = int_azione(I0,  [I1|Piano], Problemi)
      ).
attuazione([Im], Im).


%** 2.  RAPPRESENTAZIONE CONOSCENZA E APPRENDIMEMNTO *******************/

%--------- cio' che l'agente puo' osservare o ricordare
import_pred osservabile(literal).
  %  osservabile(?L) :  L rappresenta una proprieta'  osservabile
  %  dall'agente;chiameremo tali proprieta'  ``osservabili''
import_pred ricordabile(literal).
  %   osservabile(?L) :  L rappresenta un accadimento che puo' far parte
  %  dei ricordi dell'agente; chiameremo tali accadimenti ``ricordabili''

%------------- cio' che l'agente assume e i problemi che puo' riscontrare
import_pred assumable(literal).
  %  assumable(?L) :  L rappresenta una proprieta' del mondo che l'agente
  %  assume in determinate circostanze, in assenza di conoscenze sul mondo
  %  chiameremo tali proprieta' assumbili
import_pred  problema(assumibile, problema).
  %  problema(+Ass, -Prob) :  in base allo stato di conoscenza dell'agente
  %  l'assunzione Ass non e' valida per via del problema Prob

%---------- la memoria dell'agente; memorizza in base dati dinamica cio'
%             che ha imparato e i suoi ricordi
:- dynamic(imparato/1).
pred imparato(literal).
  %  imparato(L): il letterale osservabile L e' stato osservato
  %  dall'agente nello stato attuale o in uno degli stati precedenti ed e'
  %  stato memorizzato (imparato)
:- dynamic(ricordo/1).
pred ricordo(literal).
  %  ricordo(L): in letterale ricordabile L rappresenta un ricordo
  %  dell'agente

%------------- l'agente conosce, osserva, ricorda, apprende, impara, sa

pred ricorda(literal).
  %  ricorda(+L):  memorizza il ricordo di L
ricorda(L) :-
	ricordo(L),!; assert(ricordo(L)).
%  apprende cio' che osserva e non sa gia'
apprende(S,L) :-
	osserva(S,L),
	not(sa(L)).
impara(L) :-
      % sa gia' L (un letterale osservato) o lo memorizza
      sa(L), !
      ;
      assert(imparato(L)).
sa(L) :-
      % sa L (un letterale osservabile) se lo conosce staticamente o
      % lo ha imparato
	conosce(L) ; imparato(L).

% -------------- implementazione di osserva_e_impara e di valuta:
pred osserva_e_impara(stato).
%  memorizza_e_impara(+S): osserva l'ambiente nel nuovo stato S e
%  memorizza (impara) tutti i letterali che osserva in S
osserva_e_impara(S) :-
      setof(X, apprende(S,X), Appresi) ->
      maplist(impara, Appresi),
      traccia(osserva_e_impara(S), appresi:Appresi)
      ;
      traccia(osserva_e_impara(S), appresi:[]).

pred valuta(list(assumibile), list(list(problema))).
%  valuta(Ass, Prob) :  Prob e' l'insieme dei
%  problemi riscontrati in Ass;
%  ATTENZIONE: questa implementazione vale nell'ipotesi
%  che ogni assunzione abbia un insieme di problemi
%  indipendenti da quelli delle altre assunzioni
valuta(Ass, Prob) :-
	aggiungi_probl(Ass,[],Prob).

%  aggiungi_probl(Ass,P1,P2) :  aggiunge a P1 i
%  problemi riscontrati in Ass, ottenendo P2
aggiungi_probl([], P, P).
aggiungi_probl([A|Ass], P1, P2) :-
	problema(A,P),!,
	aggiungi_probl(Ass,[P|P1],P2).
aggiungi_probl([_A|Ass], P1, P2) :- !,
	aggiungi_probl(Ass,P1,P2).




/********************************************************************
	UTILITY PER LA TRACCIATURA DI DEBUGGING
*********************************************************************/

traccia(A,B) :-
	traccia_attiva,!,
	write_traccia(A,B),
	readln(R),
	(   R=[h|_], help_trace;
	    R=[d|_], no_traccia;
	    R=[a|_], abort ;
	    R=[sp|_], show;
	    R=[np|_], noshow;
	    R=[sa|_], verbose;
	    R=[na|_], nonverbose;
	    R=[t|_], trace ;
	    true
	).
traccia(_,_).

help_trace :- maplist(writeln, [
	'h-elp, a-bort, t-race, d-isattiva traccia',
	'sp (show planning), np (noshow planning)',
	'sa (show abduction), na (noshow abduction)'
	 ]).


write_traccia(A,trc(B)) :- !,
	maplist(write, [A,':    ']),
	call(trc(B)).
write_traccia(A,B) :-
	maplist(write, [A,':\n    ', B, '\n-----------']).

:- dynamic(traccia_attiva/0).

traccia :-
	writeln('------------------  ATTIVATA TRACCIA INTERATTIVA'),
	writeln('Comandi possibili durante la traccia:'),
	help_trace,
	writeln('------------------------------------------\n'),
	assert(traccia_attiva).
no_traccia :- retractall(traccia_attiva).

writeln_indent(K,X) :-
	write_indent(K,X), nl.
write_indent(K,X) :-
	forall(between(1,K,_), write(' ')),
	write(X).
write_stato(st(S,F), Msg) :-!,
	maplist(write, ['\n-------------STATO ', st(S,F), '  ', Msg, ':\n']).
write_stato(_S, _Msg) :-!,
	writeln('Stato Interruzione mostrato sopra').

write_causa(C) :-
	maplist(write, ['\n causa: ', C]).


trc(exit:decidi(S0, Decisione)) :-
	write_stato(S0, 'Presa Decisione'),
	maplist(writeln_indent(5), Decisione),
	writeln('----------------------------------------------').

trc(call:decisione(S1,_SF)) :-
	write_stato(S1, 'DECIDO ...'),
	nl.

trc(call:riconsidera(S, Piano, Causa, _Decisione)) :-
	write_stato(S, 'RICONSIDERO il piano interrotto'),
	maplist(writeln_indent(5), Piano),
	write_causa(Causa),
	writeln('----------------------------------------------').

trc(exit:calcola_piano(S0, S1, Piano)) :-
        maplist(write,
		['\npasso: ', S0, ' -> ', S1, '\nPiano:\n']),
	maplist(writeln_indent(5), Piano),
	writeln('----------------------------------------------').


trc(exit:attuabile(A, I0, I1, Costo, Assunzioni)) :-
	maplist(write,
		['\nattuabile ',A,' : ',I0, ' -> ', I1, '\n']),
	maplist(write, ['con costo ', Costo, '  e assunzioni:\n']),
	maplist(writeln_indent(5),Assunzioni),
	writeln('----------------------------------------------').

trc(exit: valuta(_Assunzioni, Problemi)):-
	maplist(write, ['\nvalutate assunzioni, problemi riscontrati: '|Problemi]),
	writeln('\n------------------').

trc(exit:esegui_azione(A,_I0,I1)):-
	maplist(write,
		['\neseguita ',A,' -> stato ', I1]),
	writeln('\n----------------------------------------------').

trc(int_azione(I0,  [I1|Piano], Problemi)) :-
	write_stato(I0, 'interruzione di azione, piano restante:'),
	maplist(writeln_indent(5), [I1|Piano]),
	writeln('Problemi ':Problemi),
	writeln('----------------------').

trc(int_piano(S0, [S1|Decisione],da_ridecidere)) :- !,
	maplist(write, ['\nInterruzione Passo di decisione:\n',
			S0, ' -> ', S1, '\nPasso da ridecidere',
		       '\nDecisione restante:\n']),
	maplist(writeln_indent(5),[S1|Decisione]),
	writeln('----------------------').

trc(int_piano(S0, [S1|Decisione],_EsitoAttuazione)) :-
	maplist(write, ['\nInterruzione Passo di decisione:\n',
			S0, ' -> ', S1, '\nCausa interruzione azione vista sopra',
		       '\nDecisione restante:\n']),
	maplist(writeln_indent(5),[S1|Decisione]),
	writeln('----------------------------------------------').
