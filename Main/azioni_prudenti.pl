h(S,H) :-
	stato_target(T),
	costo_n(S,T,H).

% azioni di spostamento
azione(va(S1,S2), stat(S1, verso_stanza(S2)), stat(S2, verso_stanza(S2)), L) :-
	connessa(S1,S2),
	non_bianca(S1),
	sa(st_bianca(S2)),
	costo_n(S1,S2,L).
azione(va(S1,S2), stat(S1, verso_stanza(S2)), stat(S2, verso_stanza(S2)), L) :-
	not(ricordo(andato(_,_))),
	connessa(S1,S2),
	non_bianca(S1),
	costo_n(S1,S2,L).
azione(va(S1,S2), stat(S1, verso_stanza(SD)), stat(S2, verso_stanza(SD)), L) :-
	S1 \= SD,
	connessa(S1,S2),
	st_bianca(S1),
	costo_n(S1,S2,L).


% azioni di visita
azione(visita(S), stat(S, cerca), stat(S, sicuro), 0) :-
	st_bianca(S).
azione(visita(S), stat(S, cerca), stat(S, pericolo), 0) :-
	non_bianca(S).
azione(visita(S), stat(S,cerca), stat(S, morto), 0) :-
	sa(wumpus(S)).
azione(visita(S), stat(S, cerca), stat(S,morto), 0) :-
	sa(pozzo(S)).


azione(cerca, stat(S, verso_stanza(S)), stat(S,cerca), 0).

% azioni di attacco
azione(uccidi_wumpus(SW), stat(S,attacco(S1)), stat(S,wumpus_ucciso), 0) :-
	wumpus(SW),
	S1 = SW.
azione(morte, stat(S,attacco(S1)), stat(S,morto), 0) :-
	wumpus(SW),
	SW \= S1.
azione(morte, stat(S,pericolo), stat(S,morto), 0) :-
	sa(non_biana(S)),
	st_rossa(S),
	not(ricordo(attaccato(S))),
	wumpus(S).
