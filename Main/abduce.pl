

/**********************  ABDUTTORE *******************************/

%  Il meta-interprete usa:
%
%  is_goal(G) :   G � una atomica goal, ovvero da dimostrare da
%                 parte del meta-interete cercando le clausole
%                 del programma meta-interpretato la cui testa
%                 unifica con G
%  assumable(A) : A � un assumibile da collezionare nelle assunzioni
%
%  not((is_goal(P); assumable(P))) : P � un predicato definito dal
%  programma indipendentemente da assunzioni: avviene call(P)


%%	abduce(+Body, -Assumibili).
%	" vi e' una prova di Body  a partire dalla lista
%	  di assumibili Ass, che utilizza il programma
%	  meta-interpretato "
%
%	verbose_flag = false:  non viene tracciata la meta
%	intrepretazione
%	cerbose_flag = true:  viene tracciata la meta-
%	interpretazione per scopi di debugging
%
abduce(B, Ass) :-
	not(verbose_flag),
	abd_body_silent(B, [], Ass1),
        clean(Ass1, [], Ass).
abduce(B, Ass) :-
	verbose_flag,
	abd_body(B, [], Ass1),
        clean(Ass1, [], Ass).

:- dynamic(verbose_flag/0).


%  comando utente per attivare la traccia
verbose :-
	verbose_flag, !; assert(verbose_flag).
%  comando utente per disattivare la traccia
nonverbose :- retractall(verbose_flag).





/************* VERBOSE *******************/
abd(true, Ass, Ass) :- !.
abd(G, Ass1, Ass2) :-
	is_goal(G),
	i_show(provo:G),
	clause(G,Body),
	abd_body(Body, Ass1, Ass2),
	show(provato:G),
	writeln('--------------------').
abd(G, Ass, [G|Ass]) :-
	assumable(G),
	show(assunto:G).
abd(G, Ass, Ass) :-
	not((is_goal(G); assumable(G))),
	show('chiamo  ':G),
	(   call(G) *->
	    show(chiamato:G)
	;   show('FAIL'), fail).


abd_body((A,B), Ass1, Ass2) :- !,
	abd_body(A, Ass1, Ass),
	abd_body(B, Ass, Ass2).

abd_body((A;B), Ass1, Ass2) :- !,
	abd_body(A, Ass1, Ass2)
	;
	abd_body(B, Ass1, Ass2).

abd_body(B, Ass1, Ass2) :-
	abd(B, Ass1, Ass2).


/*********************  SILENT *************************/
abd_silent(true, Ass, Ass) :- !.
abd_silent(G, Ass1, Ass2) :-
	is_goal(G),
	clause(G,Body),
	abd_body_silent(Body, Ass1, Ass2).
abd_silent(G, Ass, [G|Ass]) :-
	assumable(G).
abd_silent(G, Ass, Ass) :-
	not((is_goal(G); assumable(G))),
	call(G).


abd_body_silent((A,B), Ass1, Ass2) :- !,
	abd_body_silent(A, Ass1, Ass),
	abd_body_silent(B, Ass, Ass2).

abd_body_silent((A;B), Ass1, Ass2) :- !,
	abd_body_silent(A, Ass1, Ass2)
	;
	abd_body_silent(B, Ass1, Ass2).

abd_body_silent(B, Ass1, Ass2) :-
	abd_silent(B, Ass1, Ass2).


/**************** UTILITIES **********************/

clean([], Ass,Ass).
clean([A|Ass], Ass1, Ass2) :-
	ins_in(A, Ass1, AssA),!,
	clean(Ass, AssA, Ass2).

ins_in(A,[],[A]).
ins_in(A, [B|Ass], [B|Ass]) :-
	A == B,!.
ins_in(A, [B|Ass1], [B|Ass2]) :-
	ins_in(A, Ass1,Ass2).


/************************   TRACCIA ********************/


i_show(_P) :-
        not(verbose_flag),!.
i_show(P) :-
	write('\n***** traccia abduzione attiva\n      '),
	show(P),
	write('---- mostro traccia (d-isattivabile): '),
	readln(R),
	(   R=[d|_] -> retractall(verbose_flag) ;true).

show(_) :-
	not(verbose_flag),!.
show(X) :-
	is_list(X),!,
	maplist(writeln,X).
show(X) :-
	writeln(X).



