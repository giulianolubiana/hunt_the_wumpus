/*******************************************************************
        IMPLEMENTAZIONE INTERFACCIA
********************************************************************/



help_me :-
	write(
'\n
-------------------------------------------------------------
mondo(<file>) - Per caricare il mondo;
      disponibili: mondo1 , mondo2
mostra_mondo  - Per visualizzare la mappa;
mod(<modalita>) - Per selezionare la modalita dell''agete:
    modalita =coraggioso     modalita=prudente'),
    writeln('
mostra_mod - per vedere la modalita dell''agente
start(ID, Trace) per attivare l''esplorazione
     dopo aver caricato un mondo;
     Trace=true traccia on per debugging
     Trace=false no traccia di debugging
help_me - Per ricevere aiuto;
--------------------------------------------------------------\n'
	).

:- help_me.

start(ID,T) :-
	start_agente(st(_,ID),T).


% carica un mondo specificato
mondo(M) :-
	consult(M),
	maplist(write, ['caricato ', M,
			'\nmostra_mondo per vederlo',
		    '\nmod(<modalita`>) per selezionare la modalita` dell''agente']).

mostra_mondo :-
	mondo(MaxX,MaxY),
	forall(between(0,MaxY,Y),
	       (forall(between(0,MaxX,X), write_p(p(X,Y))),
	       write('\n'))).


mod(M) :-
	consult(M),
	maplist(write,  ['caricata modalita`  ', M,
		              '\nstart(ID,Tr) per partire dalla stanza identificata da ID\n']).

mostra_mod :- mod.
/*************************************************************************************
	Questa parte dell'interfaccia si occupa di rendere piu` leggibile l'output,
	rappresentato della sequenza di "mosse" eseguite dall'agente
*************************************************************************************/

write_azione(A,stat(st(_R,ID),F)) :-
    (A=morte;
	 A=inizio),
	(F=cerca;
	 F=pericolo;
	 F=sicuro;
	 F=morto;
	 F=wumpus_ucciso),
	maplist(write, [A, ' -->  ', F, '(S', ID, ')\n']).
write_azione(A, stat(st(R,ID),F)) :-
	A=visita(st(R,ID)),
	F=morto,
	maplist(write, ['visita(S', ID, ') --> ', F, '\n']).
write_azione(A, stat(st(_R,ID),F)) :-
	A=cerca,
	F=cerca,
	maplist(write, ['visita(S', ID, ') --> ', F, '\n']).
write_azione(A,stat(_S,F)) :-
	A=va(st(_R1,ID1),st(_R2,ID2)),
	F=verso_stanza(st(_R3,ID3)),
	maplist(write, ['va(S', ID1, ',S', ID2, ') --> verso_stanza(S', ID3, ')\n']).
write_azione(A,stat(_S,F)) :-
	A=va(st(_R1,ID1),st(_R2,ID2)),
	F=cerca,
	maplist(write, ['va(S', ID1, ',S', ID2, ') --> ', F, '(S', ID2, ')\n']).
write_azione(A,stat(st(R,ID),F)) :-
	A=visita(st(R,ID)),
	(F=pericolo;
	 F=sicuro),
	maplist(write, ['visita(S', ID, ') --> ', F, '(S', ID, ')\n']).
write_azione(A,stat(st(_R,ID),F)) :-
	A=uccidi_wumpus(st(R,ID1)),
	F=attacco(st(R,ID1)),
	maplist(write, ['uccidi_wumpus(S', ID1, ') --> attacco(S', ID,',S', ID1,')\n']).
write_azione(A,stat(_S,F)) :-
	A=uccidi_wumpus(st(_R,ID1)),
	F=wumpus_ucciso,
	maplist(write, ['uccidi_wumpus(S', ID1, ') --> wumpus_ucciso\n']).

write_azione(A,S) :-
	maplist(write, [A, ' -->  ', S, '\n']).





