:- op(1160, fx, type).
:- op(1160, fx, open_type).
:- op(1160, fx, predefined_type).
:- op(1160, fx, pred).
:- op(1160, fx, import_pred).
:- op(1120, xfx, ':=').
:- style_check(-discontiguous).

:- use_module('../tpl/syntax').

:- load_files([abduce,
	      architettura_agente,
	      interfaccia,
	      '../Ricerca/searchmr',
	      '../Ricerca/default',
	      '../Ricerca/astar',
	      '../Ricerca/pota_chiusi',
	      '../Ricerca/debugger']).

:- consult(abduce).

/********* QUANTO SEGUE COSTITUISCE UNA POSSIBILE IMPLEMENTAZIONE
           APERTA DEL RAGIONAMENTO BASATO SU ASSUNZINI E DELLA
	   PIANIFICAZIONE ***************************************/

/*****************************************************************
   1)   Ragionamento basato su assunzioni che usa l'abduttore
    per implementare "attuabile"
    a)  l'abduzione avviene meta-interpretando il predicato
    azione,che andra' definito per lo specifico agente ;
    b) la verifica delle assunzioni assume che una contraddizione
    riguardi una singola assunzione
    ************************************************************/

%------------   ABDUZIONE su attuabile

import_pred azione(azione, stato,stato, costo).
%  azione(?A, +S1, ?S2, -C) :  in base alle assunzioni di
%  default vi e' una azione A da S1 a S2 con costo C;
%  NB:  la query deve restituire A, S2, C ground

attuabile(Azione, S1, S2, C, Assunzioni) :-
     abduce(azione(Azione,S1,S2,C), Assunzioni).

%   A)  predicati da meta-interpretare
is_goal(azione(_,_,_,_)).
%  da aggiungere eventuali altri predicati da
%  meta-interpretare

%  B)  assumibili:  VEDI ARCHITETTURA GENERALE, sono
%  i medesimi

/**** IMPLEMENTAZIONE DELLA PIANIFICAZIONE CON A* **********/

%  i predicati che seguono si interfacciano all'algoritmo
%  di ricerca che utilizza A* con euristica h
%
:- dynamic(stato_target/1).
trovato(S) :-
     stato_target(S).

vicini(S,L) :-
     setof(V, arco(S,V), L), !.
vicini(S,[]) :-
	traccia(calcolo_piano, warning:vicini(S,[])).

arco(S1,S2) :-
     attuabile(_A,S1,S2,_C, Assunzioni),
     valuta(Assunzioni,[]).
     %  un'azione NON viene scelta se la valutazione delle
     %  assunzioni porta ad una lista non vuota di problemi

costo(S1,S2,C) :-
     attuabile(_A,S1,S2,C, _Assunzioni).

import_pred h(stato, number).
%   EURISTICA, DA IMPLEMENTARE IN BASE AL PROBLEMA


% --------- il calcolo del piano usa solve

calcola_piano(S1, S2, Piano) :-
     retractall(stato_target(_)),
     assert(stato_target(S2)),
     solve(S1, Sol),  % chiamata di search con A*
     estrai_piano(Sol, Piano).
estrai_piano(nc(S, Path, _C), Piano) :-
     reverse([S|Path], [_|Piano]).


/************************************************************************
     IMPLEMENTAZIONE DELLA GEOMETRIA DEL MONDO
*************************************************************************/

type punto := p(integer,integer).
%p(x,y) quadratino di ascissa x e ordinata y

type rettangolo := rect(punto,punto).
% rect(A,B) rettangolo che ha un vertice in A e il suo opposto in B
%
% rect(p(2,1), p(8,3))
%    0123456789...x
%   0
%   1  XXXXXXX
%   2  XXXXXXX
%   3  XXXXXXX
%   ...
%   y
%


% --------------------------------------------------------------------------
% Implementazione Oggetti del Mondo

type id := integer.
%codice identificativo di una stanza rappresentato da un intero

type colore := verde; rosso; bianco.
% colori che possono avere le stanze; saranno rosse se si trovano
% intorno alla stanza del wumpus; verdi se intorno ad un pozzo;
% altrimenti bianche.

type stanza := st(rettangolo, id).
% le stanze sono costituite da un rettangolo ed un numero che le
% identifica

type direzione := nord, sud, est, ovest.
% -------------------------------------------------------------------------
% PREDICATI PER LA RAPPRESENTAZIONE DEL MONDO

pred mondo(integer,integer).
% world(MaxX,MaxY). I punti del mondo saranno:
% da 0 a MaxX e
% da 0 a MaxY rispettivamente

pred info(stanza,colore).
% info(+S,-Col) : in base alla stanza ci viene restituito un
% un'informazione sul suo colore.

pred vicini_eo(stanza,stanza).
% vicini_o(S1,S2). Vero se le stanza S1 e S2 sono adiacenti sul piano
% orizzontale est-ovest

pred vicini_ns(stanza,stanza).
% vicini_ns(S1,S2). Vero se le stanza S1 e S2 sono adiacenti sul piano
% verticale est ovest.

pred adiacenti(stanza,stanza).
% vicini(S1,S2). Vero se S1 e` adiacente a S2

pred conn_st(stanza,list(stanza)).
% conn_st(+S,?LS). LS contiene la lista di tutte le stanza a cui e'
% connessa la stanza S

pred connessa(stanza,stanza).
% connessa(+S1,+S2). Verifica che la stanza S1 e` connessa alla stanza
% S2

pred st_random(stanza,stanza).
% random(+S1,-SR). SR e` una stanza adiacente a S1 scelta casulamente

pred pos_rel(stanza,stanza,direzione).
% pos_rel(+S1,+S2,-D). D posizione in cui si trova S2 relativamente a S1
%
pred destinazione(stanza,direzione,integer,stanza).
% destinazione()

pred wumpus(stanza).
% wumpus(?S1). Vera se nella stanza S1 e` presente il wumpus.
:- dynamic(wumpus/1).

pred pozzo(stanza).
% pozzo(?S). Indica che nella stanza S si trova un pozzo.

pred conf_id(stanza,stanza,stanza,stanza).
% conf_id(+ID1,+ID2,+ID3,-IDm): confronta ID1 e ID2 con ID3 reistituendo
% in IDm quello che dista meno

% -----------------------------------------
% Implementazione ragionamento geometrico
% -----------------------------------------
punto(p(X,Y)) :-
	mondo(X1,Y1),
	between(0,X1,X),
	between(0,Y1,Y).
% predicato che determina se un punto appartiene
% o meno al mondo

% determina se una stanza si trova direttamente
% alla destra di un'altra
vicini_eo(st(_R,ID),st(_R1,ID1)) :-
	ID1 is ID+1.

% determina se una stanza si trova direttamente alla sinistra di
% un'altra
vicini_eo(st(_R,ID), st(_R1,ID1)) :-
	ID1 is ID-1.

% determina se una stanza si trova direttamente sopra un'altra
vicini_ns(st(rect(p(X,Y1), p(X1,Y2)),_), st(rect(p(X,Y3), p(X1,Y4)),_)) :-
	Y3 is Y1-4,
	Y4 is Y2-4.

%determina se una stanza si trova direttamente sotto un'altra
vicini_ns(st(rect(p(X,Y1),p(X1,Y2)),_),st(rect(p(X,Y3),p(X1,Y4)),_)) :-
	Y3 is Y1+4,
	Y4 is Y2+4.

% determina se una stanza e` genericamente vicino ad un'altra
adiacenti(S1,S2) :-
	vicini_eo(S1,S2)
	;
	vicini_ns(S1,S2).

% determina se una stanza e` connessa ad un'altra
connessa(S1,S2) :-
	conn_st(S1,LS),
	member(S2,LS).


% wrapper che mi rende piu` leggibile il fatto che una stanza sia bianca
st_bianca(S) :-
	info(S,bianco).
% wrapper che mi rende piu` leggibile il fatto che una stanza sia rossa
st_rossa(S) :-
	info(S,rosso).
% wrapper che mi rende piu` leggibile il fatto che una stanza sia verde
st_verde(S) :-
	info(S,verde).
% wrapper che mi rende piu` leggibile il fatto che una stanza non sia
% bianca
non_bianca(S) :-
	st_rossa(S)
	;
	st_verde(S).

% distanza euclidea
euclid(st(rect(p(X1,Y1),p(X2,Y2)),_ID1), st(rect(p(X3,Y3),p(X4,Y4)),_ID2), D) :-
	D is sqrt((X1-X3)^2 + (Y1-Y3)^2),
	D is sqrt((X2-X4)^2 + (Y2-Y4)^2).

% distanza di manhattan contando le stanze
manhattan(st(rect(p(X,Y),_P),_), st(rect(p(X1,Y1),_P1),_), D) :-
	A is abs(X-X1) + abs(Y-Y1),
	A mod 4 =:= 0,
	A mod 9 \= 0,
	D is A / 4.
manhattan(st(rect(p(X,Y),_P),_), st(rect(p(X1,Y1),_P1),_), D) :-
	A is abs(X-X1) + abs(Y-Y1),
	A mod 9 =:= 0,
	D is A / 9.


% stanza random
st_random(S,SR) :-
	conn_st(S,LS),
	visita_ns(LS,LS1),
	length(LS1,N),
	N > 0,
	random(0,N,R),
	nth0(R,LS1,SR).
st_random(S,SR,false) :-
	conn_st(S,LS),
	visita_ns(LS,[]),
	length(LS,N),
	random(0,N,R),
	nth0(R,LS,SR).

st_somma(st(rect(p(A,B),p(C,D)),ID1),N,ovest,st(rect(p(A1,B),p(C1,D)),ID2)) :-
	 A1 is A + (N*9),
	 C1 is C + (N*9),
	 ID2 is ID1+N.

st_somma(st(rect(p(A,B),p(C,D)),ID1),N,est,st(rect(p(A1,B),p(C1,D)),ID2)) :-
	 A1 is A - (N*9),
	 C1 is C - (N*9),
	 ID2 is ID1-N.

st_somma(st(rect(p(A,B),p(C,D)),ID1),N,sud,st(rect(p(A,B1),p(C,D1)),ID2)) :-
	 calc_width(st(rect(p(A,B),p(C,D)),ID1),M),
	 B1 is B + (N*4),
	 D1 is D + (N*4),
	 ID2 is ID1+(N*M).

st_somma(st(rect(p(A,B),p(C,D)),ID1),N,nord,st(rect(p(A,B1),p(C,D1)),ID2)) :-
	 calc_width(st(rect(p(A,B),p(C,D)),ID1),M),
	 B1 is B - (N*4),
	 D1 is D - (N*4),
	 ID2 is ID1-(N*M).

% calcolo larghezza labirinto
meno(st(_,ID1), st(_,ID2), M) :-
	M is abs(ID1-ID2).
max(st(_,ID1), [st(_,ID2)], M) :-
	meno(st(_,ID1),st(_,ID2), M).
max(st(_,ID1), [st(_,ID2)|T], M) :-
	meno(st(_,ID1),st(_,ID2), M),
	max(st(_,ID1),T,M1),
	M > M1.
max(st(_,ID1), [st(_,ID2)|T], M1) :-
	meno(st(_,ID1),st(_,ID2), M),
	max(st(_,ID1),T,M1),
	M =< M1.

calc_width(st(R,ID),M) :-
	conn_st(st(R,ID),LS),
	max(st(R,ID),LS, M).


% posizione relativa
pos_rel(st(rect(p(X,Y),_P),_), st(rect(p(X1,Y1),_P1),_), est) :-
	A is X-X1,
	Y = Y1,
	A > 0.
pos_rel(st(rect(p(X,Y),_P),_), st(rect(p(X1,Y1),_P1),_), ovest) :-
	A is X-X1,
	Y = Y1,
	A < 0.
pos_rel(st(rect(p(X,Y),_P),_), st(rect(p(X1,Y1),_P1),_), sud) :-
	A is Y-Y1,
	X = X1,
	A < 0.
pos_rel(st(rect(p(X,Y),_P),_), st(rect(p(X1,Y1),_P1),_), nord) :-
	A is Y-Y1,
	X = X1,
	A > 0.

% varie euristiche
costo_n(S1,S2,D) :-
       manhattan(S1,S2,D).

/************************************************************************
% RAGIONARE SULLE POSIZIONI DI UNA MAPPA
************************************************************************/

% PUNTI APPARTENENTI AD UNA STANZA:
%    1) punto appartenente al perimetro di una stanza
in_prect_v(p(X,Y)) :-
	st(rect(p(X1,Y1),p(_,Y2)),_),
	between(Y1,Y2,Y),
	X=X1.
in_prect_v(p(X,Y)) :-
	st(rect(p(_,Y1),p(X2,Y2)),_),
	between(Y1,Y2,Y),
	X=X2.
%    2) punto appartenente all'area interna di una stanza
in(p(X,Y),rect(p(X1,Y1),p(X2,Y2))) :-
	between(X1,X2,X),
	between(Y1,Y2,Y).

in_prect_o(p(X,Y)) :-
	st(rect(p(X1,Y1),p(X2,_)),_),
	between(X1,X2,X),
	Y=Y1,
	not(p_corr_v(p(X,Y))).
in_prect_o(p(X,Y)) :-
	st(rect(p(X1,_),p(X2,Y2)),_),
	X > X1,
	X < X2,
	Y=Y2.


punto(p(X,Y)) :-
	mondo(MaxX,MaxY),
	between(0,MaxX,X),
	between(0,MaxY,Y).

% punto genericamante appartenente ad una stanza
p_st(p(X,Y)) :-
	st(rect(p(X1,Y1),p(X2,Y2)),_),
	in(p(X,Y),rect(p(X1,Y1),p(X2,Y2))),
	not(X=X1),
	not(X=X2),
	not(Y=Y1),
	not(Y=Y2).

% punto appartenente ad un corridoio orizzontale tra due stanze
p_corr_o(p(X,Y)) :-
	st(rect(P1,p(X1,Y1)),ID1),
	st(rect(p(X2,Y2),P2),ID2),
	vicini_eo(st(rect(P1,p(X1,Y1)),ID1),st(rect(p(X2,Y2),P2),ID2)),
	conn_st(st(rect(P1,p(X1,Y1)),ID1),LS),
	member(st(rect(p(X2,Y2),P2),ID2),LS),
	between(X1,X2,X),
	Y is Y1-1.

% punto appartenente ad un corridoio verticale tra due stanze
% (se non mettessi i due controlli finali su Y1, Y2 e Y allora mi
% verrebbero stampate delle linee verticali in mezzo ad ogni stanza)
p_corr_v(p(X,Y)) :-
	st(rect(P1,p(X1,Y1)),ID1),
	st(rect(p(X2,Y2),P2),ID2),
	vicini_ns(st(rect(P1,p(X1,Y1)),ID1),st(rect(p(X2,Y2),P2),ID2)),
	conn_st(st(rect(P1,p(X1,Y1)),ID1),LS),
	member(st(rect(p(X2,Y2),P2),ID2),LS),
	X is X2+3, Y1 < Y2, Y2=Y.

% punto che viene usato per rappresentare il numero identificativo di
% ogni stanza
p_id(p(X,Y),ID) :-
	st(rect(_,p(X1,Y1)),ID),
	X is X1-3,
	Y is Y1-1,
	not(p_wumpus(p(X,Y))).

% se l'ID e` maggiore di nove essendoci una cifra in piu` da
% rappresentare ho la necessita` di "sopprimere" un punto
% immeddiatamente dopo; cioe`:
%     ' ' -> ''
p_idM(p(X,Y)) :-
	st(rect(_,p(X1,Y1)),ID),
	X is X1-1,
	Y is Y1-1,
	not(p_wumpus(p(X,Y))),
	ID > 9.

p_wumpus(p(X,Y)) :-
	wumpus(st(rect(_,p(X1,Y1)),_)),
	X is X1-4,
	Y is Y1-1.

p_pozzo(p(X,Y)) :-
	pozzo(st(rect(_,p(X1,Y1)),_)),
	X is X1-4,
	Y is Y1-1.

% punto non libero
non_libero(P) :-
	p_st(P),
	p_corr_o(P),
	p_corr_v(P),
	(ground(P), not(punto(P))).

libero(P) :-
	punto(P),
	not(non_libero(P)).


write_p(P) :-
	in_prect_o(P) -> write('_')
	;
	in_prect_v(P) -> write('|')
	;
	p_corr_o(P) -> write('-')
	;
	p_corr_v(P) -> write('|')
	;
	p_id(P,ID) -> write(ID)
	;
	p_wumpus(P) -> write('W')
	;
	p_pozzo(P) -> write('P')
	;
	p_idM(P) -> write('')
	;
	write(' ').

/***********************************************************************************
     STATI DEL CACCIATORE
************************************************************************************/

type fluent :=
        verso_stanza(stanza);
	attacco(stanza);
%	sente_russare;
	morto;
	cerca;
	wumpus_morto.

fluent(F) :-
	F=verso_stanza(_);
	F=attacco(_);
%	F=sente_russare;
	F=cerca;
	F=morto;
	F=wumpus_ucciso.

type stato := stat(stanza, fluent).

type azione :=
	morte;
	cerca;
	va(stanza,stanza);	 % va da una stanza ad un'altra ad essa connessa
	visita(stanza);
	uccidi_wumpus(stanza).

type stato_decisione :=
	   successo; fallimento;
	   {stato}; {interruzione}.

type problema := contr(assumibili,osservabile).

/********************************************************************************
	ARCHITETTURA DEL CACCIATORE
*********************************************************************************/

%------------- PREDICATI DI DECISIONE ED ESECUZIONE
pred obiettivo(stato_decisione).
% obbiettivo(+S):    S e' lo stato che rappresenta l'obiettivo che deve
%                    portare a termine il cacciatore.
pred riconsidera(stato, list(stato), interruzione_decisione, list(stato)).
% riconsidera(+S, +Restante, +Causa, -Decisione)
% Pre:  l'esecuzione di una decisione e' stata interrotta nello stato S,
%       Restante e' la sequenza di decisioni non attuata
%       Causa e' la causa dell'interruzione
% Post: Decisione e' la nuova sequenza di decisioni
pred prendi_decisione(stato, list(stato)).
% prendi_decisione(+S,-Dec): Dec e' la sequenza di decisioni presa nello
%			     stato S in base alla conoscenza attuale
%			     dell'agente.
pred esegui_azione(azione,stato,stato).
% esegui_azione(A,S1,S2): l'agente esegue in modo effettivo l'azione A,
% passando dallo stato S1 allo stato S2, imparando quanto osserva ed
% eventualmente modificando il mondo.

%-------------- CONOSCENZE ASSUNZIONI
pred osservabile(atomic).
% osservabile(?L): L e' una proprieta' osservabile
pred ricordabile(atomic).
% osservabile(?L): L e' un accadimento ricordabile
pred osserva(stato,literal).
% osserva(+S,?L) : l'agente osserva nello stato S che L e' una
% proprieta' vera del mondo
pred conosce(literal).
% conosce(+S,?L) : L fa parte delle conoscenze statiche (a priori) che
% l'agente ha del mondo
pred assumable(literal).
% assumable(?L) : L rappresenta una proprieta' del mondo che l'agente
% assume in determinate circostanze, in assenza di conoscenze sul mondo
% chiameremo tali proprieta' assumibili
pred problema(assumibile,problema).
% problema(+Ass, -Prob) : in base allo stato di conoscenza dell'agente
% l'assunzione Ass non e' valida per via del problema Prob.
pred visita_ns(stanza,list(stanza),list(stanza)).
% visita_ns(+S,+LS1,-LS2) : dove LS2 e` la lista delle stanze a cui e`
% connessa S privata delle stanze gia` visitate.
pred stanza_vicina(stanza,list(stanza),stanza).
% confrondo_id_min(+S,+LS,-S1). Dove S1 e` la stanza che si
% avvicina maggiormente alla stanza S, tra tutte le stanza
% contenute in LS.
pred riconsidera(stato, list(stato), interruzione_decisione, list(stato)).
% riconsidera(+S, +Restante, +Causa, -Decisione)
% Pre: l'esecuzione di una decisione e` stata interrotta nello stato S,
%      Restante e` la sequenza di decisioni non attuata; Causa e` la
%      causa dell'interruzione
% Post:Decisione e` la nuova sequenza di decisioni

% ------------- Pronte da usare
pred calcola_piano(stato, stato, list(stato)).
% calcola_piano(+S1, +S2, -Piano): Piano e' un piano per passare da S1
% ad S2, valutato nello stato di conosenza in S1.
pred attuabile(azione, stato, stato, number, list(assumibile)).
% attuabile(?A, +S1, ?S2, ?C, ?Ass) : assumendo ASS, vi e' un'azione A
% che passa dallo stato S1 allo stato S2 con costo C.


% --------------- gestione della conoscenza: cio' che l'agente conosce
% --------------------------------------------------------------------
% cio' che il cacciatore assume in prima istanza; cioe':
% 1) una stanza a priori non ha connessioni
% 2) non ci sono wumpus
% 3) tutte le stanze sono bianche
assumable(no_conn).
assumable(no_wumpus).
assumable(st_bianca(_)).

% problemi (contraddizioni) che falsificano le assunzioni fatte sopra:
%    1) se si assume che una stana e' bianca, ma si e' a conoscenza per
%       esperienza diretta che quella stanza non lo e';
problema(st_bianca(S),contr(st_bianca(S),non_bianca(S))) :-
	sa(non_bianca(S)).
%    2) se si assume che non ci sono wumpus, quindi non ce n'e'
%       nemmeno nella stanza S, ma si constata che ve n'e' uno nella
%	stanza S senza averla visitata;
problema(no_wumpus, contr(no_wumpus, wumpus(S))) :-
	sa(wumpus(S)),
	not(ricordo(visitato(S))).
%    3) se si assume che la stanza S1 non e' collegata alla stanza S2,
%	ma si sa che questa connessione esiste senza aver visitato ne'
%	una ne' l'altra.
problema(no_conn, contr(no_conn, connessa(S1,S2))) :-
	sa(connessa(S1,S2)),
	not(ricordo(visitato(S1))),
	not(ricordo(visitato(S2))).

% in pratica e` una remove delle stanze gia` visitate
visita_ns([],[]).
visita_ns([H|L],L1) :-
	ricordo(visitato(H)),
	visita_ns(L,L1).
visita_ns([H|L],[H|L1]) :-
	not(ricordo(visitato(H))),
	visita_ns(L,L1).

distanze(_S,[],[]).
distanze(S,[H|T],[(H,D)|SM]) :-
	euclid(S,H,D),
	distanze(S,T,SM).
min([T],T).
min([(S,D)|T], (S,D)) :-
	min(T,(_,D1)),
	D =< D1.
min([(_S,D)|T], (S1,D1)) :-
	min(T,(S1,D1)),
	D1 < D.
st_vicina(S,S1,SR) :-
	conn_st(S,LS),
	visita_ns(LS,NLS),
	distanze(S1,NLS,LST),
	min(LST,(SR,_D)).
% cio' che il cacciatore puo' ricordare; cioe':
%     1) che si e' mosso da una stanza ad un'altra
ricordabile(andato((rect(_,_),_),(rect(_,_),_))).
%     2) che ha visitato una certa stanza
ricordabile(visitato((rect(_,_),_))).
%     3) che ha iniziato l'attacco ad una stanza in cui assume che ci
%        sia il Wumpus
ricordabile(attaccato(_)).

% cio' che il cacciatore puo' osservare; cioe':
osservabile(connessa(_,_)).     %     1) che una stanza e' collegata con un'altra
osservabile(st_bianca(_)).      %     2) che una stanza e' bianca
osservabile(non_bianca(_)).     %     3) che una stanza e' rossa o verde
osservabile(wumpus(_)).         %     4) che una stanza contiene un wumpus
osservabile(pozzo(_)).          %     5) che una stanza contiene un pozzo
% osservabile(sente_russare(_)). % 6) che in una stanza si sente russare

% cio' che il cacciatore osserva; cioe':
% 1) che una stanza S e' connessa ad una stanza S1, se il cacciatore
%	 e' in uno stato in cui e' nella stanza S e non e' morto, le due
%	 stanze sono vicine ed effettivamente sono connesse.
osserva(stat(S,F), connessa(S,S1)) :-
	fluent(F),
	F \= morto,
	connessa(S,S1).

% 2) che una stanza S e' bianca, quindi sicura per l'agente, se e'
%	 in quella stanza, non e' morto ed effettivamente la stanza e'
%	 bianca.
osserva(stat(S,F), st_bianca(S)) :-
	fluent(F),
	F \= morto,
	st_bianca(S).
% 3) che una stanza S non e' bianca, quindi non sicura per
%	 l'agente, se e' in uno stato in cui si trova all'interno della
%	 stanza stessa e la stanza e' effettivamente di un colore
%	 diverso dal bianco.
osserva(stat(S,F), non_bianca(S)) :-
	fluent(F),
	F \= morto,
	non_bianca(S).
% 4) il cacciatore ha modo di osservare che un wumpus e' presente in
%	 una stanza solamente se si trova nella sua stessa stanza;
osserva(stat(S,F), wumpus(S)) :-
	fluent(F),
	wumpus(S).
% 5) il cacciatore ha modo di osservare che e` presente un pozzo
%	 in una certa stanza S soltanto se anche lui si trova in quella
%	 stanza S.
osserva(stat(S,F), pozzo(S)) :-
	fluent(F),
	pozzo(S).
% 6) il cacciatore ha modo di "osservare" che nella stanza S si sente russare
/*osserva(stat(S,F), sente_russare(S)) :-
	fluent(F),
	info(S,	LS),
	member(russare,LS).*/

conosce(L) :-
	catch(conosce_nel_mondo_caricato(L), _, fail).

/***************************************************************************
       PREDICATI DI DECISIONE
****************************************************************************/

% per adesso faccio partire il cacciatore con nessuna conoscenza, se mi
% accorgo che mi serve allora tornero' indietro. Ma se riuscissi a
% mantenerlo cosi' il mio agente manterrebbe la massima generalita'.

obiettivo(stat(_,wumpus_ucciso)).
obiettivo(stat(_,morto)).
% il cacciatore si puo' trovare in una stanza qualsiasi e deve aver
% ucciso il Wumpus; oppure fallisce.

import_pred riconsidera(stato, list(stato), interruzione_decisione, list(stato)).

riconsidera(S, _, da_ridecidere, Decisione) :-
	prendi_decisione(S,Decisione).
riconsidera(_S, _, int_azione(I0, _, _), Decisione) :-
	prendi_decisione(I0,Decisione).




:- dynamic(eseguita/3).

esegui_azione(cerca, stat(S,F1), stat(S,F2)) :-
	assert(eseguita(cerca, stat(S,F1), stat(S,F2))).
esegui_azione(visita(S), stat(S,F1), stat(S,F2)) :-
	assert(eseguita(visita(S), stat(S,F1), stat(S,F2))),
	ricorda(visitato(S)).
esegui_azione(va(S1,S2), stat(S1,F1), stat(S2,F2)) :-
	assert(eseguita(va(S1,S2), stat(S1,F1), stat(S2,F2))),
	ricorda(andato(S1,S2)).

esegui_azione(va(S1,S2), stat(S1,F1), stat(S1,F2)) :-
	assert(eseguita(va(S1,S2), stat(S1,F1), stat(S1,F2))).
esegui_azione(uccidi_wumpus(S2), stat(S1,F1), stat(S1,F2)) :-
	assert(eseguita(uccidi_wumpus(S2), stat(S1,F1), stat(S1,F2))),
	ricorda(attaccato(S2)).
esegui_azione(morte, stat(S,F1), stat(S,F2)) :-
	assert(eseguita(morte, stat(S,F1), stat(S,F2))).

start_agente(S,Trace) :-
	writeln('Start, se hai caricato il mondo o e` cambiato -> a-bort'),
	readln(R),
	(   R=[a|_] -> abort; true),
	(   Trace=true ->
	       traccia,
	       count_on
	       ;
	       no_traccia,
	       count_off
	),
	retractall(eseguita(_,_,_)),
	retractall(imparato(_)),
	retractall(ricordo(_)),
	decisione(stat(S,cerca),_),
	mostra_esecuzione(stat(S,cerca)).

mostra_esecuzione(S) :-
	write_azione(inizio,S),
	forall(eseguita(A,_S1,S2), write_azione(A,S2)).






