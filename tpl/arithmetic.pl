prototype(aritmetic, integer, [_], body).
prototype(aritmetic, number, [_], body).
prototype(aritmetic, float, [_], body).

prototype(arithmetic, Op, [integer, integer], integer) :-
	member(Op,[+, -, *, //, mod]).
prototype(arithmetic, Op, [real, real], real) :-
	member(Op,[+, -, *, /]).
prototype(arithmetic, Op, [integer, real], real) :-
	member(Op,[+, -, *, /]).
prototype(arithmetic, Op, [real, integer], real) :-
	member(Op,[+, -, *, /]).
prototype(arithmetic, /, [integer, integer], real).

prototype(arithmetic, N, [], integer) :-
	integer(N).
prototype(arithmetic, N, [], real) :-
	float(N).
