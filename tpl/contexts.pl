:- module(contexts, [ins_var/3,
		     ins_proto/3,
		     empty_context/1,
		     clean_context/2
		    ]).
:- use_module(ld, [sub_type/2,
		   prototype/3]).

% type context := cnt(I:int, T: list(types), V: list(vars), P:
% list(proto))
%      T lista di tipi ben definiti, con possibili varabili di tipo
%      V  lista di variabili tipizzate su T U predefiniti
%      P  lista di prototipi definiti su T U predefiniti


/******************* CONTESTI *************************/

%%	empty_context(?C:context)
%       C � il contestovuoto

empty_context(cnt([],[],[])).

%%	ins_var(+TV:typed_var, +Cnt1:context, -Cnt2:context)
%       post: TV = X:Type, Cnt2 = Cnt1 U X:Type
%             l'inserimento pu� produrre istanziazioni di
%             variabili di tipo del contesto
%
ins_var(X:TX, cnt(T,V,P), cnt(T1,V1,P)) :-
	term_variables(TX,VT),
	ins_type_vars(VT, T, T1),
	ins(X:TX, V, V1).


%%	ins_proto(+P:[def_prototype|new_prototype],
%		  +Cnt1:context, -Cnt2:context)
%       post: Cnt2 = Cnt1 U P
%             l'inserimento pu� produrre istanziazioni di
%             variabili di tipo del contesto
%
ins_proto(Proto, cnt(T,V,P), cnt(T,V,P1)) :-
	ins_in_protolist(Proto,P,P1).
ins_proto(Proto, cnt(T,V,P), cnt(T,V,P1)) :-
	ins_in_protolist(Proto,P,P1).


clean_context(cnt(T, V, P), cnt(T1, V1, P1)) :-
	filter_vars(T, TT),
	remove_duplicates(TT,T1),
	filter_vars(V, VV),
	remove_duplicates(VV,V1),
	check_prototypes(P,P1).

check_prototypes([def(F,Types,atomic,head)|L], L1) :- !,
	head_generality_check(F,Types, ResList) *->
	      check_prototypes(L, LL),
	      append(ResList,LL,L1)
	      ;
	      FA =..[F|Types],
	      throw(err(FA:atomic,[prototype_error(head)])).
check_prototypes([def(F,Types,T,body)|L], L1) :-
	(   T==atomic; T==body ), !,
	body_generality_check(F,Types,T, ResList) *->
	      check_prototypes(L, LL),
	      append(ResList,LL,L1)
	      ;
	      FA =.. [F|Types],
	      throw(err(FA:T,[prototype_error(body)])).
check_prototypes([def(F,Types,T1,T)|L], L1) :-
	fun_check(F,Types,T1) *->
	      check_prototypes(L, L1)
	      ;
	      FA =.. [F|Types],
	      throw(err(FA:T1,[prototype_error(T)])).
check_prototypes([N|L], [N1|L1]) :-
	new_check(N,N1),
	      check_prototypes(L, L1).
check_prototypes([],[]).

new_check(new(F,Types,T), non_dichiarato(FT:T)) :- !,
	FT =.. [F|Types].
new_check(N, non_chiaro(N)).

fun_check(F,Types,T1) :-
	prototype(F, AF, T1),
	subsumes_term(AF,Types).
head_generality_check(F, A, []) :-
	prototype(F, AF, atomic),
	A =@= AF.
head_generality_check(F, A, [warning(more_general_head:FA, FAF)]) :-
	prototype(F, AF, atomic),
	subsumes_term(A, AF),
	FA =.. [F|A],
	FAF =.. [F|AF].
body_generality_check(F, A, T, []) :-
	prototype(F, AF, T),
	A =@= AF.
body_generality_check(F, A, T, [warning(less_general_body:FA, FAF)]) :-
	prototype(F, AF, T),
	subsumes_term(AF, A),
	FA =.. [F|A],
	FAF =.. [F|AF].

filter_vars([], []).
filter_vars([E:_T|L], L1) :-
	not(var(E)), !,
	filter_vars(L,L1).
filter_vars([V|L], [V|L1]) :-
	filter_vars(L,L1).

remove_duplicates([], []).
remove_duplicates([H|L], L1) :-
	mem(H,L), !,
	remove_duplicates(L,L1).
remove_duplicates([H|L],[H|L1]) :-
	remove_duplicates(L,L1).
mem(H, [K|_L]) :-
	H==K,!.
mem(H,[_|L]) :-
	mem(H,L).


%%	used by ins_var; a typed variable is inserted
%
ins(X:T, [], [X:T]).
ins(X:T, [Y:T1|L], [X:MT|L]) :-
	X==Y, !,
	min_type(X,T,T1,MT).
ins(XT, [H|L], [H|L1]) :-
	ins(XT, L,L1).

min_type(X,T1,T2,T3) :-
	(   unify_with_occurs_check(T1,T2) -> T3=T2
	;   sub_type(T1,T2) -> T3=T1
	;   sub_type(T2,T1) -> T3=T2
	;   throw(err(X, [type_clash(X:T1,X:T2)] ))).

%%	used by ins_var, inserts type variables of X:T in the
%	type part of the context C, to guarantee that
%	C |- T:type
%
ins_type(T, [], [T:type]).
ins_type(T, [V:Type|L], [V:Type|L]):-
	T == V,
	Type == type.
ins_type(T, [H|L], [H|L1]) :-
	ins_type(T,L,L1).

ins_type_vars([],G,G).
ins_type_vars([T|TT], G0, G) :-
	ins_type(T,G0,G1),
	ins_type_vars(TT,G1,G).


%%	ins_in_protolist(+P,+L1,-L2)
%       L2 = L1 U P
%       le variabili di tipo possono essere istanziate
%
ins_in_protolist(N, [], [N]).
ins_in_protolist(new(F, Types, Type), [new(F1, Types1, Type1)|L],
	[new(F, Types, Type)|L]) :-
	F==F1,
	Types = Types1,
	Type = Type1,
	!.
ins_in_protolist(def(F, Types, Type1, Type), [def(PF, PTypes, PType1, PType)|L],
	[def(F, Types, Type1, Type)|L]) :-
	F==PF,
	Types = PTypes,
	Type1 = PType1,
	Type = PType,
	!.
ins_in_protolist(N, [H|L], [H|L1]) :-
	ins_in_protolist(N,L,L1).
