:- module(prolog, [
	      predefined_goal/1,
	      prolog_operator/4,
	      prolog_proto/3
	  ]).

prolog_operator(relational,700,xfx, Rel) :-  member(Rel,[
      <, =, (=..), (=@=),  (=:=), (=<), (==), (=\=),  >, (>=), (@<),
      (@=<), (@>), (@>=), (\=), (\==), is
					 ]).
prolog_operator(additive, 500,yfx, AddOp) :-  member(AddOp,[
     +, -, (/\), (\/), xor
					 ]).
prolog_operator(multiplicative,400,yfx, MulOp) :-  member(MulOp,[
     *, /, (//), rdiv, (<<), (>>), mod, rem
					 ]).
prolog_operator(exp, 200,xfy, ExpOp) :-  member(ExpOp,[
     (**), (^)
					 ]).
prolog_operator(prefix,200,fy, PrefixOp) :-  member(PrefixOp,[
     (+), (-), (/)
					 ]).


predefined_goal(G) :-
	G =.. [Op,_,_],
	prolog_operator(relational, _,_,Op).
predefined_goal(open(_,_,_)).
predefined_goal(write(_)).
predefined_goal(writeln(_)).
predefined_goal(write(_,_)).
predefined_goal(read(_)).
predefined_goal(readln(_)).
predefined_goal(read(_,_)).
predefined_goal(nl).
predefined_goal(nl(_)).
predefined_goal(assert(_)).
predefined_goal(asserta(_)).
predefined_goal(assertz(_)).
predefined_goal(retract(_)).
predefined_goal(retractall(_)).
predefined_goal(at_end_of_stream(_)).
predefined_goal(maplist(_,_)).
predefined_goal(maplist(_,_,_)).
predefined_goal(CALL) :-
	not(var(CALL)),
	CALL =.. [call|_].


prolog_proto(Op, [T,T], body) :-
	prolog_operator(relational, _,_,Op).

prolog_proto(write, [_], body).
prolog_proto(writeq, [_], body).
prolog_proto(writeln, [_], body).
prolog_proto(write, [stream, _], body).
prolog_proto(writeq, [stream, _], body).
prolog_proto(nl, [stream], body).
prolog_proto(nl, [], body).
prolog_proto(read, [_], body).
prolog_proto(readln, [list(_)], body).
prolog_proto(read, [stream, _], body).
prolog_proto(assert, [clause], body).
prolog_proto(asserta, [clause], body).
prolog_proto(assertz, [clause], body).
prolog_proto(retract, [clause], body).
prolog_proto(retractall, [clause], body).
prolog_proto(maplist, [_, list(_)], body).
prolog_proto(maplist, [_, list(_), list(_)], body).
prolog_proto(call, [body], body).

prolog_proto(open, [file_name, file_opening, stream], body).
prolog_proto(exists_file, [file_name], body).
prolog_proto(close, [stream], body).
prolog_proto(at_end_of_stream, [stream], body).
prolog_proto(forall, [body, body], body).
prolog_proto(setof, [T,body,list(T)], body).

prolog_proto(read, [], file_opening).
prolog_proto(write,[], file_opening).

