:- module(tc, [
	term/3,
	found_error/2
   ]).

:- use_module(ld, [
		  prototype/3,
		  meta_prototype/3,
		  sub_type/2
	      ]).

:- use_module(contexts, [ ins_var/3,  %tried
			 ins_proto/3,
			 clean_context/2,  %tried
			 empty_context/1]).


/**** 0. Metapredicato ausiliario: try, se eccezione rileva errore e fallisce
*****************************************************************************/

:- dynamic(wff_error/2).   % base dati dinamica errori individuati
found_error(X,Y) :- wff_error(X,Y).
clear_tc_errors :-  retractall(wff_error(_,_)).

try(A) :-
    catch(A, Err, (report(Err), fail)),
         % se tutti i tentativi hanno sollevato eccezioni d'errore,
	 % si salva un report nella base dati dinamica e si ha fallimento
    retractall(wff_error(_,_)).
         % se almeno un tentativo non ha sollevato eccezione d'errore
	 % si ha successo e i tentativi errati sono ignorati
report(err(Term, Details)) :-
	assert(wff_error(Term, Details)).



/**** 1.  TYPE CHECKING ********************************************/

%%	term(+C1:context, -C2:context, +Term:term, ?T:type)
%	C2-C1 |- Term:T
%       possibili piu' soluzioni in caso di overloading;
%       fallisce in caso d'errore e la base dati
%       wff_error contiene tutti i report d'errore
%
term(C, Term, Type) :-
	clear_tc_errors,
	empty_context(C0),
	try(try_term(C0, C1, Term, Type)),
	try(clean_context(C1,C)),
	not(found_error(_,_)).

%  1.1. il termine e' una variabile: Cn0 U X:T |- X : T

try_term(Cn0, Cn, X, T) :-
	var(X), !,
	try(ins_var(X:T,Cn0,Cn)).

%  1.2. Term = F(Args),  F(Types) � un generatore di T
%      Il prototipo F(Types) � salvato nel contesto come
%      definito (def) se dichiarato,nuovo (new) altrimenti,
%      per il controllo finale
%
%      Cn-Cn0 |- Args:Types
%      -------------------
%      Cn-Cn0 |- F(Args):T
%
try_term(Cn0, Cn, Term, T) :-
	destructure(Term, Args, _F, Types, T, FoundProto),
	are_terms(Cn0,Cn1,Args,Types),
	add_prototype(FoundProto, Cn1, Cn)
	    *-> true
	;
	throw(err(Term, [term(T)])).


%  1.3. verifica  Cn-Cn0 |- Args:Types
%
are_terms(Cn,Cn,[],[]).
are_terms(Cn0,Cn,[A|AA],[T|TT]):-
	try( try_term(Cn0,Cn1,A,T) ),
	are_terms(Cn1,Cn,AA,TT).


/****************************** AUX **********************************/


%%	destructure(+Term:term, -T:type, -Args:list(terms),
%                    -Types:list(types),
%                    -FoundProto: [new_protptype|def_prototype]).
%	Term = F(Args) e
%	  a) F(Types) � un generatore di T1<=T dichiarato e
%	     FoundProto = def(F,Types,T1,T) , oppure
%	  b) F(Types) non verifica a) e %
%	     FoundProto = new(F,Types,T)
%
destructure(Term, Args, F, Types, Type, FoundProto) :-
	Term =.. [F|Args],
	same_length(Args,Types),
	(   prototype(F, Types, Type1), sub_type(Type1,Type) *->
	    FoundProto = def(F,Types,Type1, Type)
	;   FoundProto = new(F, Types, Type)).


%%	add_prototype(+P:[new_prototype|def_prototype],
%                     +C1:context, -C2:context))
%	post1:  P � un prototipo sintattico o un generatore di
%               un tipo individuale e C1=C2
%	post2: non vale il caso precedente e C2 si ottiene da C1
%	aggiungendo P ai prototipi ricostruiti
%


% 1.  i prototipi sintattici non sono aggiunti
add_prototype(def(F, Types, Type1, _Type), G, G) :-
	meta_prototype(F, Types, Type1), !.
% 2.  i prototipi definiti di tipi non meta non sono aggiunti
add_prototype(def(F, Types, Type1, _Type), G, G) :-
	prototype(F, Types, Type1),
	not(member(Type1,[atomic, body, clause])), !.
% 3. tutto il resto � aggiunto e verr� filtrato alla fine
add_prototype(FoundProto, G, G1) :-
	ins_proto(FoundProto, G, G1).





