:- module(ld, [
	    load_prog/1,      % LOADING
	    unload_prog/1,
	    clear_loaded_types/0,

	    defined_clause/1, % SINTASSI
	    prototype/3,
	    meta_prototype/3,
	    sub_type/2,

	    prove/1,	      % TRACCIA
	    defaults/0,
	    verbose_skip/0,
	    silent_skip/0,
	    skip_p/1,
	    noskip_p/1,
	    goals_tr/0,
	    goals_notr/0
	  ]).

:- use_module(prolog, [predefined_goal/1,
		       prolog_proto/3]).

:- use_module(help, [trace_help/0, trace_commands/0]).

:- dynamic([
       g_prototype/3,
       loaded_library/1
	   ]).

:- multifile([prototype/4]).

/***  1. Segnatura   ***********************************************/

% 1.1.	prototipi sintassi astratta

meta_prototype({}, [head], clause).
meta_prototype(':-', [head, body], clause).
meta_prototype(',', [body, body], body).
meta_prototype(';', [body, body], body).
meta_prototype('->', [body, body], body).
meta_prototype({}, [atomic], body).
meta_prototype({}, [atomic], head).
meta_prototype(not, [body], body).
meta_prototype(once, [body], body).
meta_prototype(true, [], body).
meta_prototype(fail, [], body).
meta_prototype('!', [], body).
meta_prototype(=, [T,T], body).
meta_prototype(\=, [T,T], body).


% 1.1.  meta-prototipi
%
prototype(F, A, T) :-
	member(F, [':-', ',', ';', '->', {}, not, true, fail, '!', '=', (\=)]),
	meta_prototype(F,A,T).

% 1.2	prototipi da librerie caricate

prototype(F, A, T) :-
	loaded_library(L),
	prototype(L,F,A,T).

% 1.3   prototipi utente
%
prototype(F, A, T) :-
	g_prototype(F,A,T).

% 1.3.1  prototipi prolog
prototype(F, A, T) :-
	prolog_proto(F, A, T).

% 1.4   la relazione di subtyping indotta dai prototipi

sub_type(T1, T2) :-
	unify_with_occurs_check(T1,T2).
sub_type(T1,T2) :-
	prototype({}, [T1], T),
	sub_type(T,T2).


% 1.5   le clausole con testa definita da pred(...).
%
defined_clause(Head:-Body) :-
	defined(Head),
	clause(Head, Body).

defined(P) :-
	declared(pred(DP)),
	DP =.. [Op|Types],
	same_length(Args,Types),
	P =.. [Op|Args].


/*** 2.  CARICAMENTO DEL PROGRAMMA UTENTE ********************************/


% 2.1.   Carica la segnatura del programma nella base dati
%        dinamica g_prototype
%
load_prog(Prog) :-
	consult(Prog),
	forall(declared(predefined_type(T)), load_types(T)),
	forall(declared(type(:=(T,G))),
	       forall(ormember(P,G), assert_proto(P,T)) ),
	forall(declared(pred(P)), assert_proto(P,atomic)),
	forall(declared(dynamic_pred(P)), assert_proto(P,atomic)),
	forall(declared(import_pred(P)),assert_proto(P,atomic)),
	forall(declared(export_pred(P)),assert_proto(P,atomic)),
	forall(declared(call_pred(P)),assert_proto(P,atomic)).

declared(D) :-
	% se una dichiarazione (di tipo, di ...) non esiste,
	% l'esito � fail, non un errore in esecuzione
	catch(D,_,fail).


% 2.2. scarica file e base dati correnti
%
unload_prog(Prog) :-
	clear_loaded_types,
	unload_file(Prog).


clear_loaded_types :-
	retractall(g_prototype(_,_,_)),
	forall(retract(loaded_library(L)),
	       unload_file(L)).

% Ausiliari per 2

ormember(X, (G1;G2)) :-
	ormember(X, G1)
        ;
	ormember(X, G2).
ormember(X, X) :-
	not(X=(_;_)).

assert_proto(P,T) :-
	P=..[Op|Types],
	assert(g_prototype(Op, Types, T)).

/**** 3.  CARICAMENTO LIBRERIE DI TIPO PREDEFINITE *******************/

type_library(T) :-
	atom(T),
	member(T, [lists, arithmetic, ordering]).

load_types(L) :-
	is_list(L) -> forall(member(T,L), ld_type(T))
	;
	ld_type(L).

ld_type(L) :-
	not(type_library(L)), !,
	writeln(L: ' non e'' la libreria di un tipo predefinito **************').
ld_type(L) :-
	not(loaded_library(L)), !,
	atom_concat('tpl/',L, LP),
	consult(LP),
	assert(loaded_library(L)).
ld_type(L) :-
	atom_concat('tpl/',L, LP),
	consult(LP),
	writeln('Warning, ricaricata la libreria di tipo ':L).





/**** 4. META INTERPRETE DI TRACCIA   ***********************************/


%  main:  prove(G) traccia la ricerca dei proof trees di G
%  e del programma attualmente caricato
%       mostra i proof-trees in formato:
%       | ? Goal da dimostrare
%       |   ...
%       |   sottoprove
%       |   ...
%       | ! Istanza del goal dimostrata
%       Se non si mostrano le assunzioni mostra solo
%       le istanze dimostrate, cio� l'albero di prova
%

prove(G) :-
	start_msg,!, % messaggio di avvio
	show_pt(G).

show_pt(G) :-
	show_body(0,G),
	nl,
	write('---------------------------( ; per vedere altri pt) '),
	readln(X),
	nl,
	X \= [;|_], % se l'utente risponde ";" riprova
	!.
show_pt(_) :-
	writeln('FINE, non ci sono altri pt').

%  A.   Goals prolog ---------------------------------------------
%
show(H,!) :- !,
	writegoal(H, ('call !  ************** NB: cut non implementato')),
	writeconcl(H, ('exit !')).
	%skip dei cut, cut non implementato

show(_,true) :- !.

show(H,fail) :- !,
	write_failure(H, fail),
        fail.

show(H,not(G)) :- !,
	writegoal(H, not(G)),
	(   call(G) ->  write_failure(H, not(G))
	;   writeconcl(H,not(G))).
	% la negazione non � tracciata, � trattata come test

show(H, once(G)) :-
	writegoal(H, once(G)),
	K is H+1,
	show_body(K, G),!,
	writeconcl(H, once(G)).

show(H,G) :-
	predefined_goal(G), !,
	writegoal(H, G),
	(   call(G) *->  writeconcl(H,G)
	;   write_failure(H, G)).

% B. Goals utente, tutti i casi prolog sono esauriti sopra
%    Si hanno 3 possibili "stati"
%    - defined, goal dichiarati e non skipped, sono metainterpretati
%    - skipped, si passa alla esecuzione non metainterpretata
%    - undefined, non dichiarati e non skipped, sono
%      imprevisti: abort

show(H, G) :-
	declared(G,State),!,
	show(State, H, G).

declared(G, State) :-
	G=..[F|A],
	same_length(A,V),
	SG=..[F|V],!,
	(   skipped(SG) -> State=skipped
	;   defined(SG) -> State=defined
	;   State=undefined).

skipped(P) :-
	(   declared(skip_pred(SP)); declared(open_pred(SP));
	    declared(dynamic_pred(SP)); skip_trace(SP) ),
	SP =.. [Op|Types],
	same_length(Args,Types),
	P =.. [Op|Args].

show(undefined,H,G) :-
	writegoal(H, '************* non dichiarato, non viene tracciato ':G),
	writegoal(H, 'call':G),
		(   call(G) *->  writeconcl(H,exit:G)
	;   write_failure(H, call:G)).

show(defined, H, G) :-
	writegoal(H, G),
	(   clause(G, Body) *->
	        K is H+1,
		show_body(K, Body)
	;   write_failure(H, G),
	    fail
	),
	writeconcl(H,G).

show(skipped, H, G) :-
	writegoal(H, call: G),
	(   call(G) *->  writeconcl(H,exit:G)
	;   write_failure(H, call:G)).

%%	Show BODY --------------------------------------------------


show_body(_K, true) :- !.
show_body(K, (A;B)) :-
	not(A=(_ -> _)),!,
	(   show_body(K, A);
	    show_body(K, B) ).
show_body(K, (A->B;C)) :- !,
	test_if(K, A, B, C).
show_body(K, (A->B)) :- !,
	test_if(K, A, B, fail).
show_body(K, (A,B)) :- !,
	show_body(K, A),
	show_body(K, B).
show_body(K,A) :-
	show(K, A).


% --------- implementazione di H -> B; C  non ancora esaustivamente
% testata
test_if(H, T, A, B) :-
	writegoal(H,'IF ***':T),
	(   call(T) ->
                writeconcl(H, 'TEST true, THEN'),
	        show_body(H,A)
		;
	        writeconcl(H, 'TEST false, ELSE: '),
		show_body(H,B)
	).


/************ AUSILIARI METAINTERPRETE:  visualizzare ******************/


writeconcl(H, _C) :-
	nascondi(K),
	H > K, !.
writeconcl(H, C) :-
	columns(H),
	write_seq(['|',H,' !  '], C),
	ask(H).

writegoal(_,_) :-
	skip_goals, !.

writegoal(H, _C) :-
	nascondi(K),
	H > K, !.

writegoal(H, G) :-
	columns(H),
	write_seq(['|', H, ' ?  '],G),
	ask(H).

write_failure(H, IN) :-
	columns(H),
	writeln('*** FALLITO':IN),
	writeln('RIPROVO ...'),!,
	fail.

columns(H) :-
	forall(between(1,H,_), write('   |')).

write_seq(S, G) :-
	append(S,[G], WG),
	maplist( write, ['   '|WG]).


/************ AUSILIARI METAINTERPRETE:  comandi ******************/

ask(K) :-
	traccia(K),!,
	write(' > '),
	readln(X),
	exec(K,X).
ask(_) :- nl.
re_ask(K) :-
	H is K+1,
	columns(H),
	write(' CONTINUA '),
	ask(K).


msg(K, M) :-
	columns(K),
	writeln(M).

nascondi(H) :-
	no_write,
	skip_level(H).

traccia(K) :-
	skip_level(L),!,
	K =< L.
traccia(_).

exec(K,[h|_]) :- !,
	trace_help,
	re_ask(K).
exec(K,[s|_]) :- !,
	not(skip_level(_)) -> assert(skip_level(K))
	;
	retractall(skip_level(_)).
exec(_,[g|_]) :-!,
	not(skip_goals) -> assert(skip_goals)
	;
	retractall(skip_goals).

exec(_,[d|_]) :- !,
	defaults.

exec(_,[a|_]) :- !,
	abort.
exec(_,[t|_]) :- !,
	trace.
exec(_,[n|_]) :- !,
	notrace.

exec(_,_).


/************ AUSILIARI METAINTERPRETE:  flag di stato e visualizzazione
              iniziale dello stato corrente
	      comandi pre-esecuzione di modifica dei flag
************************************************************************/

:- dynamic(skip_goals/0).
:- dynamic(skip_level/1).
:- dynamic(no_write/0).
:- dynamic(skip_trace/1).


start_msg :-
	show_state,
	forall(skip_trace(P), writeln(traccia_off(P))),
	write('\n RET per partire, a-bort per abortire -------------------->'),
	readln(C),
	(   C=[a|_] -> abort ; true).

show_state :-
	writeln('--------------------------  stato corrente dei flags'),
	(   skip_level(K) -> maplist(write,
	    ['  s-kip(', K, ') on: step by step "skipped" ai livelli > ',K,'\n'])
	;   writeln('  skip off [default], esegue tutto passo passo')
	),
	(   skip_goals -> writeln('  g-oal off, non mostra i goals')
	;    writeln('  g-oal on [default], mostra i goals')),
	(   no_write -> writeln('  v-isualizza off, non traccia i livelli s-kipped')
	;   writeln('  v-isualizza on [default], traccia i livelli s-kipped')).


defaults :-
	retractall(skip_level(_)),
	retractall(skip_trace(_)),
	retractall(skip_goals),
	retractall(no_write).
verbose_skip :-
	retractall(no_write).
silent_skip :-
	assertonce(no_write).
skip_p(P) :-
	assertonce(skip_trace(P)).
noskip_p(P) :-
	retractall(skip_trace(P)).
goals_tr :-
	retractall(skip_goals).
goals_notr :-
	assertonce(skip_goals).

assertonce(P) :-
	P,!; assert(P).





