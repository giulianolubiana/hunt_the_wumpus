:- module(syntax, [
	      op(1160, fx, type),
	      op(1160, fx, open_type),
	      op(1160, fx, predefined_type),
              op(1160, fx, pred),
	      op(1160, fx, dynamic_pred),
	      op(1160, fx, import_pred),
	      op(1160, fx, export_pred),
	      op(1160, fx, called_pred),
              op(1120, xfx, ':='),
	      op(90, xfx, '..')
	  ]).


