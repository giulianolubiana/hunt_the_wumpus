:- op(1199, fx, type).
:- op(1199, fx, pred).
:- op(1110, xfy, (:=)).

/*  DEDFINISCE:      solve/2,
		     solve_target/3,
		     target/1
*/

% type list(X) := [ ]  ;  [X | list(X)]. predefinito

%%%%   TIPO DEFINITO DALLA STRATEGIA
%      TNP  tipo nodi problema, aperto
type frontiera(_TNP).

%%    TIPO per la rappresentazione interna dei nodi
%
type nodo(TNP) := nc(TNP,list(TNP),number).

%%    TIPO DIPENDENTE DAL PROBLEMA: quando il target non �
%     definito staticamente ma fa perte dell'istanza di
%     problema (vedere predicati path/3 e target/1)
type info_target.


%%%%%%%% PREDICATI DEFINITI DAL PROBLEMA.

pred trovato(_TNP).
   %  trovato(+N) is semidet:    N e' un goal, dipende dal problema
pred vicini(TNP, list(TNP)).
   % vicini(+N, -L) is det:   L e' la lista dei "vicini di L"
   %                   (collegati ad L da un arco)
pred costo(TNP, TNP, number).
   % costo(+N1,+N2,-C) is det: C e' il costo dell'arco (N1,N2)

pred h(_TNP, number).
   % h(+N,-H) is det:  N e' il nodo corrente, H e' la stima
   % euristica del costo da N ad una soluzione ottimale


%%%%   PREDICATI DEFINITI DALLA FRONTIERA E DALLA STRATEGIA

pred starting.
%  starting definito dalla strategia per inizializzare eventuali
%  basi dati o files; per lo pi� non fa nulla

pred frontiera_iniziale(nodo(TNP),frontiera(TNP)).
   % frontiera_iniziale(+N,-F) is det:   F e' la frontiera con il solo
   %  nodo N.

pred scelto(nodo(TNP), frontiera(TNP), frontiera(TNP)).
  % scelto(-N, +F0,-F1) is det: N e' un nodo di F0 (il nodo selezionato)
  %			 e F1 e' F0 senza N;
pred aggiunta(list(nodo(TNP)), frontiera(TNP), frontiera(TNP)).
   % aggiunta(+L, +F1, -F2) is det:      F2  si ottiene aggiungendo L ad F1

pred taglia(TNP, list(TNP)).
  %  taglia(S,L) is semidet:
  %  A)	vero se lo stato S o uno stato
  %     che include le soluzioni raggiungibili da S e' gia' stato
  %     incontrato in L (cammino dal nodo corrente alla radice)
  %  B) Nel caso in cui si chiudano i nodi incontrati, invece � vero
  %     se TNP appartiene ai chiusi.

pred chiudi(_TNP).
  %  A) se non si mantiene una lista di nodi chiusi per la potatura,
  %  ha successo senza effetti;
  %  B) altrimenti aggiunge TNP alla base dati dei nodi chiusi; in tal
  %  caso taglia_cicli taglia piu' pesantemente i nodi chiusi


%% PREDICATI DEFINITI DAL PROGRAMMA

pred solve(TNP, nodo(TNP)).
  % solve(+Start,nc(-Goal,-Path,-Cost)) is nondet:   da Start si raggiunge
  %    Goal attraverso il cammino Path con costo Cost; Goal e' una
  %    soluzione

pred  solve_target(TNP, info_target, nodo(TNP)).
  % solve_target(+Start,+Target,nc(-Goal,-Path,-Cost)) is nondet:
  %	 a) Target e' una informazione usabile da trovato per
  %      stabilire i nodi di successo rispetto al Target
  %      b)  da Start si raggiunge nodo target attraverso il cammino
  %      Path con costo Cost
pred target(info_target).
  % target(-Info) is det:  Info serve a "trovato" per
  %     determinare le soluzioni in base al target
  %     dell'istanza di problema; � un'informazione passata al
  %     momento della chiamata con path

pred cerca(frontiera(nodo(TNP)),nodo(TNP)).
  %  cerca(+F,-N) is nondet:      vi e' un cammino da un nodo di F ad un goal
  %  inizialmente F=nodo iniziale, poi ricorsivamente altre frontiere

pred trasforma(list(TNP),nodo(TNP),list(nodo(TNP))).
   % trasforma(+Vicini,+N,-F) is det:   F e' la porzione di
   %        frontiera contenente i Vicini, da aggiungere
   %	    alla frontiera corrente

%************************************************************************
%

incr(N) :-
	nb_getval(count,N),
	M is N+1,
	nb_setval(count,M).

solve_target(G, L, S) :-
	retractall(target(_)),
	assert(target(L)),
	solve(G,S).
:- dynamic(target/1).

solve(N,G) :-
      starting,
      frontiera_iniziale(nc(N,[],0),F0),
      nb_setval(count,0),
      cerca(F0,G),
      nb_getval(count, Steps),
      writeln('\n**************************** STEPS ':Steps).


/************** L'ALGORITMO GENERICO *********************************/

cerca(Frontiera, nc(PN, Path, Cost)) :-
       scelto(nc(PN, Path, Cost),Frontiera,_),
       trovato(PN),                      % dal problema
       (   showflag -> mostra(Frontiera),command; true).

cerca(Frontiera, Goal) :-
	incr(_),
	(   showflag -> mostra(Frontiera),command; true),
	scelto(nc(N,Path,Cost),Frontiera,F1),	 % dalla strategia
        vicini(N,Vicini),		         % dal problema
        trasforma(Vicini,nc(N,Path,Cost),FrontieraVicini),
	chiudi(N),
        aggiunta(FrontieraVicini,F1,NuovaFrontiera), % dalla strategia
	cerca(NuovaFrontiera,Goal).



trasforma([],nc(_,_,_),[]).
trasforma([V|T], nc(N,Path,Cost),TT) :-
        taglia(V,[N|Path]),!,
        trasforma(T,nc(N,Path,Cost),TT).
trasforma([V|T], nc(N,Path,Cost),[nc(V,[N|Path],Cost1)|TT]) :-
        costo(N,V,K),
	Cost1 is Cost+K,
        trasforma(T,nc(N,Path,Cost),TT).














