
:- dynamic(chiuso/1).
%  Usa una base dati dinamica chiuso/1 per memorizzare i nodi
%  chiusi (cioe' gia' incontrati);  non  fa controlli sui costi,
%  ovvero assume la consistenza dell'euristica.
%  NB:   se il grafo � finito e i suoi nodi sono noti in partenza,
%  la chiusura di un nodo si riduce ad un flag associato al nodo;
%  quando il nodo � espanso, si alza il flag; un nodo � chiuso
%  se ha il flag alzato

starting :-
	  retractall(chiuso(_)).

%% best_eq_node(N1,N2) e' usata solo da frontiera_ord; controlla se N1 e
%  N2 contengono lo stesso nodo problema; se ha successo, N3 � il nodo
% con costo minore fra i due da sovrascrivere se = non � l'eguaglianza
% fra nodi usato per confrontare i nodi in frontiera
best_eq(nc(N1,P1,C1),nc(N2,P2,C2), nc(N2,P,C)) :-
	N1=N2,!,
	(   C1 < C2 -> P=P1, C=C1; P=P2, C=C2   ),
	(   showflag -> writeln(best_eq(N1:C1, N2:C2, nc(N2,P,C))); true).

% se si vuole disattivare best_eq: commentare sopra e scommentare sotto:
% best_eq(_,_,_) :- fail.

%%  taglia  implementato come taglio dei chiusi
taglia(N,_) :-
    (	showflag -> writeln(verifica_chiusura(N)); true),
    chiuso(N),
    (	showflag -> writeln(N:' � chiuso'); true).

chiudi(N) :-
    (	showflag -> writeln(chiudo(N)); true),
    (	not(chiuso(N)) -> assert(chiuso(N)); true ).







