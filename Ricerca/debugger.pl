


/********************     INTERFACCIA DEBUGGING   ****************/
:- dynamic(showflag/0).

show :-
	%nb_setval(count,0),
	assert(showflag),
	writeln('.... entro in modalita'' interattiva'),
	sh.
sh :- writeln('comandi:	 n: noshow; a: abort; t: trace; h: help').
noshow :-
	retractall(showflag).
command :-
	%incr(N),
	nb_getval(count,N),
	write(N),
	write(' |:'),
	readln(R),
	(
	R = [], !
	;
	R = [h|_], !, sh, command
	;
	R = [n|_], !, noshow
	;
	R = [t|_], !, trace
	;
	R = [a|_], !, abort
        ;
	true
	).


