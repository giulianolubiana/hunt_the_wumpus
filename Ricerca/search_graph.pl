:- op(1199, fx, type).
:- op(1199, fx, meta_type).
:- op(1199, fx, pred).
:- op(1110, xfy, (:=)).

/*  DEDFINISCE:      solve/2,
		     solve_target/3,
		     target/1
*/

% type list(X) := [ ]  ;  [X | list(X)]. predefinito

%%%%   TIPO DEFINITO DALLA STRATEGIA
%      TNP  tipo nodi problema, aperto
type frontiera(_TNP).

%%    TIPO per la rappresentazione interna dei nodi
%
type nodo(TNP) := nc(TNP,list(TNP),number).

%%    TIPO DIPENDENTE DAL PROBLEMA: quando il target non �
%     definito staticamente ma fa perte dell'istanza di
%     problema (vedere predicati path/3 e target/1)
type info_target.


%%%%%%%% PREDICATI DEFINITI DAL PROBLEMA.
%
%
%

meta_type grafo(nodo(TNP), pred:(TNP), pred:(TNP,list(TNP)) ,
	       pred(TNP,TNP, number), ped:(TNP, number)).

%   grafo(Source, Target, Vicini, Costo, H) ha il seguente
%   significato
%

%  Source � il nodo di partenza del grafo di ricerca
%
pred Target:(TNP).
   %  call(Target,+N) is semidet:    N e' un goal,
pred Vicini:(TNP, list(TNP)).
   % call(Vicini, +N, -L) is det:   L e' la lista dei "vicini di L"
   %                  (collegati ad L da un arco)
pred Costo:(TNP, TNP, number).
   % call(Costo, +N1,+N2,-C) is det: C e' il costo dell'arco (N1,N2)
pred H:(_TNP, number).
   % call(H, +N,-H) is det:  N e' il nodo corrente, H e' la stima
   % euristica del costo da N ad una soluzione ottimale


%%%%   PREDICATI DEFINITI DALLA FRONTIERA E DALLA STRATEGIA

pred starting.
%  starting definito dalla strategia per inizializzare eventuali
%  basi dati o files; per lo pi� non fa nulla

pred frontiera_iniziale(nodo(TNP),frontiera(TNP)).
   % frontiera_iniziale(+N,-F) is det:   F e' la frontiera con il solo
   %  nodo N.

pred scelto(strategia,  nodo(TNP), frontiera(TNP), frontiera(TNP)).
  % scelto(-N, +F0,-F1) is det: N e' un nodo di F0 (il nodo selezionato)
  %			 e F1 e' F0 senza N;
pred aggiunta(strategia, grafo, list(nodo(TNP)), frontiera(TNP), frontiera(TNP)).
   % aggiunta(+L, +F1, -F2) is det:      F2  si ottiene aggiungendo L ad F1

pred taglia(TNP, list(TNP)).
  %  taglia(S,L) is semidet:
  %  A)	vero se lo stato S o uno stato
  %     che include le soluzioni raggiungibili da S e' gia' stato
  %     incontrato in L (cammino dal nodo corrente alla radice)
  %  B) Nel caso in cui si chiudano i nodi incontrati, invece � vero
  %     se TNP appartiene ai chiusi.

pred chiudi(_TNP).
  %  A) se non si mantiene una lista di nodi chiusi per la potatura,
  %  ha successo senza effetti;
  %  B) altrimenti aggiunge TNP alla base dati dei nodi chiusi; in tal
  %  caso taglia_cicli taglia piu' pesantemente i nodi chiusi


%% PREDICATI DEFINITI DAL PROGRAMMA

pred solve(grafo(_Start,_Target,_Vicini,_Costo,_H), nodo(_TNP)).
  % solve(+Graph,nc(-Goal,-Path,-Cost)) is nondet:
  %    nel grafo di ricerca definito da Graph, da Start si raggiunge
  %    un  nodo Target attraverso il cammino Path con costo Cost; Goal e' una
  %    soluzione

pred cerca(grafo(_Start,_Target,_Vicini,_Costo,_H),
	   frontiera(nodo(TNP)),nodo(TNP)).
  %  cerca(+Graph, +F,-N) is nondet:      nel grafo Graph,
  %  vi e' un cammino da un nodo di F ad un goal
  %  inizialmente F=nodo iniziale, poi ricorsivamente altre frontiere

pred trasforma(grafo(_Start,_Target,_Vicini,_Costo,_H),
	       list(TNP),nodo(TNP),list(nodo(TNP))).
   % trasforma(+Vicini,+N,-F) is det:   F e' la porzione di
   %        frontiera contenente i Vicini, da aggiungere
   %	    alla frontiera corrente

%************************************************************************
%

incr(N) :-
	nb_getval(count,N),
	M is N+1,
	nb_setval(count,M).

solve(grafo(N,Target,Vicini,Costo,H), G) :-
      starting,
      frontiera_iniziale(nc(N,[],0),F0),
      nb_setval(count,0),
      cerca(grafo(N,Target,Vicini,Costo,H),F0,G),
      nb_getval(count, Steps),
      writeln('\n**************************** STEPS ':Steps).


/************** L'ALGORITMO GENERICO *********************************/

cerca(grafo(_S,Target,_Vicini,_Costo,_H),Frontiera, nc(PN, Path, Cost)) :-
       scelto(nc(PN, Path, Cost),Frontiera,_),
       call(Target, PN),                      % dal problema
       (   showflag -> mostra(Frontiera),command; true).

cerca(grafo(S,Target,PVicini,Costo,H),Frontiera, Goal) :-
	incr(_),
	(   showflag -> mostra(Frontiera),command; true),
	scelto(grafo(S,Target,Vicini,Costo,H),
	       nc(N,Path,Cost),Frontiera,F1),	 % dalla strategia
        call(PVicini, N, Vicini),		         % dal problema
        trasforma(grafo(S,Target,PVicini,Costo,H),
		  Vicini,nc(N,Path,Cost),FrontieraVicini),
	chiudi(N),
        aggiunta(grafo(S,Target,PVicini,Costo,H),
		 FrontieraVicini,F1,NuovaFrontiera), % dalla strategia
	cerca(grafo(S,Target,PVicini,Costo,H),NuovaFrontiera,Goal).



trasforma(_,[],nc(_,_,_),[]).
trasforma(GR,[V|T], nc(N,Path,Cost),TT) :-
        taglia(V,[N|Path]),!,
        trasforma(GR,T,nc(N,Path,Cost),TT).
trasforma(grafo(S,Target,PVicini,Costo,H),
	  [V|T], nc(N,Path,Cost),[nc(V,[N|Path],Cost1)|TT]) :-
        call(Cost,N,V,K),
	Cost1 is Cost+K,
        trasforma(grafo(S,Target,PVicini,Costo,H),T,nc(N,Path,Cost),TT).














