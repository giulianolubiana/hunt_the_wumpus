:- module(mr, [help_me/0,
	       strategy/1,
	       solve/2,
	       solve_target/3,
	       target/1,
	       show/0,
	       noshow/0]
	 ).

/******************  INTERFACCIA DIDATTICA ********************************/
/****** NB:  IL PROGETTO DOVRA' FORNIRE LA 'SUA' INTERFACCIA !! ***********/

/********** Schermata iniziale ************************/
:- writeln(
'*****************************************************************************
STRATEGIE RICERCA DISPONIBILI:
    profondita, ampiezza, astar, costo_minimo, best_first,
    [taglio_cicli, <strategia>],
    [pota_chiusi, <strategia>]'
),
writeln('help_me.  per help\n
*****************************************************************************'),
consult(searchmr),
consult(debugger).


/********************** HELP *************************/
help_me :- maplist(writeln,[
'*****************************************************************************
STRATEGIE RICERCA DISPONIBILI:
    profondita, ampiezza, astar, costo_minimo, best_first,
    [taglio_cicli, <strategia>],
    [pota_chiusi, <strategia>]'
,
'comandi:
    strategy(+S: nome strategia)
    solve(+Start: nodo_problema, -Soluzione: nc)
    solve_target(+Start:nodo_problema, +T:target, -S: nc)
       in questo caso "trovato" usa target(T)',
'    show  [per debugging]
    noshow',
'Problema:  deve definire
    trovato(?nodo).     trovato(N) : N e'' un goal
    vicini(+nodo,-list(nodo)).   vicini(N,L) : L lista dei vicini di N
    Se vi e'' un costo non unitario:
       costo(+nodo, +nodo, -number). costo(N1,N2,C) : C costo di N1->N2
    Se la strategia usa un''euristica:
       h(+nodo, -number).   h(N,E) : E valore euristico di N'
,
'*****************************************************************************'
	]).

/*********************  Comandi utente **************************************/

%%	scelta strategia
strategy(S)
:-
(   is_list(S) -> maplist(s_consult,S); s_consult(default), s_consult(S)),
maplist(write, ['\n*********************       CARICATA STRATEGIA ',S, '       ****************\n\n']).

s_consult(F) :-
	catch(problem_specific(F, RF), _, fail), !,
	writeln(F:'RIDEFINITO'),
	load_files(RF).
s_consult(F) :-
	atom_concat('Ricerca/',F, RF),
	load_files(RF,[silent(true)]).

/********************* FINE INTERFACCIA DIDATTICA **************************/

